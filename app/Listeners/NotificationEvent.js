'use strict';

const ShopFollowRepository = use('App/Repositories/ShopFollowRepository');
const JoinEventRepository = use('App/Repositories/JoinEventRepository');
const TokenRepository = use('App/Repositories/TokenRepository');
const Config = use('Config');
const { type, sender, target } = Config.get('notification');
const { sendNotificationToAndroid, saveNotification, formatDateTime } = use('App/Helpers');
class NotificationEvent {
  constructor() {
    this.shopFollowRepo = new ShopFollowRepository();
    this.tokenRepo = new TokenRepository();
    this.joinEventRepo = new JoinEventRepository();
  }

  /**
   * sendToFollowers
   * @param event
   * @param shop
   * @return {Promise.<void>}
   */
  async sendToFollowers({ event, shop }) {
    const followers = await this.shopFollowRepo.getListFollower(shop.id);
    const ids = [];
    followers.forEach((follow) => {
      ids.push(follow.user_id);
    });
    const deviceTokens = await this.tokenRepo.getDeviceTokensByIds(ids);
    const data = {
      type: type.shop_create_new_event,
      event,
    };
    const typeSender = sender.event;
    const eventName = `${event.name.slice(0, 15)}...`;
    const content = `${shop.name} tổ chức sự kiện ${eventName}`;
    for (let i = 0; i < deviceTokens.length; i += 1) {
      saveNotification({
        data,
        sender: shop.id,
        receiver: deviceTokens[i].user_id,
        typeSender,
        avatar: shop.avatar,
        content,
        targetType: target.event,
        targetId: event.id,
      });
      const notificationData = {
        deviceToken: deviceTokens[i],
        data,
        tag: `${event.id}_${shop.id}`,
        body: content,
        avatar: shop.avatar,
        title: shop.name,
      };
      sendNotificationToAndroid(notificationData);
    }
  }

  /**
   * sendEventInDay
   * @param event
   * @param shop
   * @return {Promise.<void>}
   */
  async sendEventInDay({ event, shop }) {
    event.images = JSON.parse(event.images);
    event.cover = event.images[0];
    event.from = formatDateTime(event.from);
    event.to = formatDateTime(event.to);
    delete event.images;
    const followers = await this.joinEventRepo.getListParticipants(event.id);
    const ids = [];
    followers.forEach((follow) => {
      ids.push(follow.user_id);
    });
    const deviceTokens = await this.tokenRepo.getDeviceTokensByIds(ids);
    const data = {
      type: type.event_will_happening,
      event,
    };
    const typeSender = sender.event;
    const eventName = `${event.name.slice(0, 15)}...`;
    const content = `Sự kiện ${eventName} sẽ diễn ra trong hôm nay`;
    for (let i = 0; i < deviceTokens.length; i += 1) {
      saveNotification({
        data,
        sender: 0,
        receiver: deviceTokens[i].user_id,
        typeSender,
        avatar: event.cover,
        content,
        targetType: target.event,
        targetId: event.id,
      });
      const notificationData = {
        deviceToken: deviceTokens[i],
        data,
        tag: `${event.id}_${shop.id}`,
        body: content,
        avatar: event.cover,
        title: shop.name,
      };
      sendNotificationToAndroid(notificationData);
    }
  }
}

module.exports = NotificationEvent;
