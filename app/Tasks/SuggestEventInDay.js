'use strict';

const Task = use('Task');
const EventRepository = use('App/Repositories/EventRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const Event = use('Event');
class SuggestEventInDay extends Task {
  static get schedule () {
    return '0 0 23 * * *';
  }

  /**
   * handle
   * @return {Promise.<void>}
   */
  async handle () {
    const eventRepo = new EventRepository();
    const shopRepo = new ShopRepository();
    const dateEvents = await eventRepo.getDateEvent();
    dateEvents.forEach(async (event) => {
      const shop = await shopRepo.getShopInfo(event.shop_id);
      Event.fire('event:sent_event_in_day', { event, shop });
    });
  }
}

module.exports = SuggestEventInDay;
