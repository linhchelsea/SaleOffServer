const EventRepository = use('App/Repositories/EventRepository');
const JoinEventRepository = use('App/Repositories/JoinEventRepository');
const PostRepository = use('App/Repositories/PostRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const ShopUserRepository = use('App/Repositories/ShopUserRepository');
const ShopNotFoundException = use('App/Exceptions/ShopNotFoundException');
const NotShopOwnerException = use('App/Exceptions/NotShopOwnerException');
const NotShopMemberException = use('App/Exceptions/NotShopMemberException');
const FromInvalidException = use('App/Exceptions/FromInvalidException');
const EventNotFoundException = use('App/Exceptions/EventNotFoundException');
const EventJoinedException = use('App/Exceptions/EventJoinedException');
const EventDailyException = use('App/Exceptions/EventDailyException');
const NotParticipantException = use('App/Exceptions/NotParticipantException');
const { formatDateTime } = use('App/Helpers');
const Event = use('Event');
class EventService {
  constructor () {
    this.eventRepo = new EventRepository();
    this.joinEventRepo = new JoinEventRepository();
    this.shopRepo = new ShopRepository();
    this.shopUserRepo = new ShopUserRepository();
    this.postRepo = new PostRepository();
  }

  /**
   * getDetail
   * @param eventId
   * @param userId
   * @return {Promise.<null>}
   */
  async getDetail(eventId, userId) {
    const event = await this.eventRepo.getDetail(eventId);
    const joinEvent = await this.joinEventRepo.getDetail(userId, eventId);
    event.is_join = joinEvent ? 1 : 0;
    event.share_code = joinEvent ? joinEvent.event_code : 0;
    event.from = formatDateTime(event.from);
    event.to = formatDateTime(event.to);
    event.images = JSON.parse(event.images);
    return event;
  }


  async getShopEvents(userId, shopId) {
    const shop = await this.shopRepo.getShopInfo(shopId);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    const users = await this.shopUserRepo.getEmployee(shopId);
    const ids = [];
    users.forEach((user) => {
      user.cares = JSON.parse(user.cares);
      ids.push(user.id);
    });
    if (shop.owner_id !== userId && ids.indexOf(userId) < 0) {
      throw new NotShopMemberException();
    }
    const events = await this.eventRepo.getShopEvents(shopId);
    events.forEach((event) => {
      event.images = JSON.parse(event.images);
      event.cover = event.images[0];
      delete event.images;
      event.from = formatDateTime(event.from);
      event.to = formatDateTime(event.to);
    });
    return events;
  }

  /**
   *
   * @param userId
   * @return {Promise.<Array>}
   */
  async getEvents(userId) {
    const ids = [];
    const listEvents = await this.joinEventRepo.getListEventYouJoined(userId);
    listEvents.forEach((event) => {
      ids.push(event.event_id);
    });
    const events = await this.eventRepo.getListEventYouJoined(ids);
    events.forEach((event) => {
      event.images = JSON.parse(event.images);
      event.cover = event.images[0];
      delete event.images;
      event.from = formatDateTime(event.from);
      event.to = formatDateTime(event.to);
    });
    return events;
  }

  async getRecentlyEvents(userId, page, type) {
    let events;
    switch (+type) {
      case 1:
        events = await this.eventRepo.getEventsChuaDienRa(page);
        break;
      case 2:
        events = await this.eventRepo.getEventsDangDienRa(page);
        break;
      default:
        events = await this.eventRepo.getEventsDaDienRa(page);
        break;
    }
    for (let i = 0; i < events.length; i += 1) {
      events[i].images = JSON.parse(events[i].images);
      events[i].cover = events[i].images[0];
      delete events[i].images;
      const joinEvent = await this.joinEventRepo.getDetail(userId, events[i].id);
      events[i].from = formatDateTime(events[i].from);
      events[i].to = formatDateTime(events[i].to);
      events[i].is_joined = joinEvent ? 1 : 0;
    }
    return events;
  }
  /**
   * joinEvent
   * @param params
   * @param userId
   * @return {Promise.<void>}
   */
  async joinEvent(params, userId) {
    let joinEvent = await this.joinEventRepo.getDetail(userId, params.event_id);
    if (joinEvent) {
      throw new EventJoinedException();
    }
    try {
      const total = await this.joinEventRepo.getTotal();
      joinEvent = await this.joinEventRepo.joinToEvent(params, userId, total + 100001);
      // share event
      const event = await this.eventRepo.getDetail(params.event_id);
      await this.postRepo.shareEvent(userId, params.event_id, event.address, params.description);
      return joinEvent;
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * createEvent
   * @param userId
   * @param params
   * @return {Promise.<*>}
   */
  async createEvent(userId, params) {
    const shop = await this.shopRepo.getShopInfo(params.shop_id);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    if (shop.owner_id !== userId) {
      throw new NotShopOwnerException();
    }
    const lastEvent = await this.eventRepo.getLastEvent(shop.id);
    if (lastEvent) {
      const time = Date.now() - lastEvent.created_at;
      if (time < 60 * 1000 * 24) { // one day
        throw new EventDailyException();
      }
    }
    const from = +(new Date(params.from)).getTime();
    const now = new Date();
    now.setHours(now.getHours() + 7);
    if (from - now.getTime() < 0) {
      throw new FromInvalidException();
    }
    const event = await this.eventRepo.createEvent(params);
    // push notification to followers
    Event.fire('event:sent_to_followers', { event, shop });
  }

  /**
   * getListParticipants
   * @param eventId
   * @return {Promise.<Array>}
   */
  async getListParticipants(eventId) {
    const joinEvents = await this.joinEventRepo.getListParticipants(eventId);
    const participants = [];
    for (let i = 0; i < joinEvents.length; i += 1) {
      const { user } = joinEvents[i];
      user.share_code = joinEvents[i].event_code;
      user.is_used = joinEvents[i].is_used;
      user.point = await this.joinEventRepo.getPoint(joinEvents[i].event_code);
      participants.push(user);
    }
    return participants;
  }

  /**
   * searchParticipants
   * @param eventId
   * @param content
   * @return {Promise.<Array>}
   */
  async searchParticipants(eventId, content) {
    const joinEvents = await this.joinEventRepo.searchParticipants(eventId, content);
    const participants = [];
    joinEvents.forEach((event) => {
      const { user } = event;
      user.share_code = event.event_code;
      user.cares = JSON.parse(user.cares);
      participants.push(user);
    });
    return participants;
  }

  /**
   * usePoint
   * @param id
   * @param userId
   * @param authId
   * @returns {Promise.<void>}
   */
  async usePoint(id, userId, authId) {
    const event = await this.eventRepo.getDetail(id);
    if (!event) {
      throw new EventNotFoundException();
    }
    const shop = await this.shopRepo.getShopInfo(event.shop_id);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    const employ = await this.shopUserRepo.getShopUser(shop.id, authId);
    if (shop.owner_id !== authId && !employ) {
      throw new NotShopMemberException();
    }
    const participant = await this.joinEventRepo.getDetail(userId, id);
    if (!participant) {
      throw new NotParticipantException();
    }
    const isUsed = await this.joinEventRepo.usePoint(participant.event_code);
    return isUsed;
  }
}

module.exports = EventService;
