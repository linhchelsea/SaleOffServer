'use strict';

const NotificationRepository = use('App/Repositories/NotificationRepository');
class NotificationService {
  constructor () {
    this.notificationRepo = new NotificationRepository();
  }

  /**
   * get
   * @param userId
   * @returns {Promise.<*>}
   */
  async get(userId) {
    const notifications = await this.notificationRepo.get(userId);
    return notifications;
  }

  /**
   * deleteNotification
   * @param id
   */
  delete(id) {
    this.notificationRepo.delete(id);
  }
}

module.exports = NotificationService;
