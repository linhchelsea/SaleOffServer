'use strict';

const ReportRepository = use('App/Repositories/ReportRepository');
class ReportService {
  constructor () {
    this.reportRepo = new ReportRepository();
  }

  /**
   * send
   * @param userId
   * @param params
   * @returns {Promise.<void>}
   */
  async send(userId, params) {
    await this.reportRepo.send(userId, params);
  }
}

module.exports = ReportService;
