'use strict';

const HomeRepository = use('App/Repositories/HomeRepository');
const PostRepository = use('App/Repositories/PostRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const ShopFollowRepository = use('App/Repositories/ShopFollowRepository');
const UserFollowRepository = use('App/Repositories/UserFollowRepository');
const ProductRepository = use('App/Repositories/ProductRepository');
const CategoryRepository = use('App/Repositories/CategoryRepository');
const PostLikeRepository = use('App/Repositories/PostLikeRepository');
const EventRepository = use('App/Repositories/EventRepository');
const CategoryNotFoundException = use('App/Exceptions/CategoryNotFoundException');
const { formatDateVN, formatDateTime } = use('App/Helpers');
class HomeService {
  constructor() {
    this.homeRepo = new HomeRepository();
    this.shopFollowRepo = new ShopFollowRepository();
    this.userFollowRepo = new UserFollowRepository();
    this.productRepo = new ProductRepository();
    this.catRepo = new CategoryRepository();
    this.postLikeRepo = new PostLikeRepository();
    this.postRepo = new PostRepository();
    this.shopRepo = new ShopRepository();
    this.eventRepo = new EventRepository();
  }

  /**
   * getPostsFollowing
   * @param user
   * @param params
   * @returns {Promise.<Array>}
   */
  async getPostsFollowing(user, params) {
    let id = params.last_id;
    if (id === 0) {
      id = await this.homeRepo.getLastPost() + 1;
    }
    const category = await this.catRepo.getCategoryById(+params.category);
    if (!category) {
      throw new CategoryNotFoundException();
    }
    const productIds = [];
    if (params.category !== 1) {
      const products = await this.productRepo.getListProductByCategory(params.category);
      products.forEach((product) => {
        productIds.push(product.id);
      });
    }
    const posts = await this.homeRepo.getFollowingNewsFeed(user.id, productIds, id, user.address);
    for (let i = 0; i < posts.length; i += 1) {
      const product = await this.productRepo.getProductById(posts[i].product_id);
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        postShared.images = JSON.parse(postShared.images);
        postShared.category_name = category.name;
        postShared.product_name = product.name;
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i] = await this.formatPost(posts[i]);
        posts[i].category_name = category.name;
        posts[i].product_name = product.name;
        posts[i].post = null;
      }
      const isLike = await this.postLikeRepo.getPostLike(posts[i].id, user.id);
      posts[i].is_like = isLike ? 1 : 0;
    }
    return posts;
  }

  async getPostsCaringFollowing(user, params) {
    let id = params.last_id;
    if (id === 0) {
      id = await this.homeRepo.getLastPost() + 1;
    }
    const productIds = [];
    const categories = JSON.parse(user.cares);
    const products = await this.productRepo.getListProductByCategories(categories);
    products.forEach((product) => {
      productIds.push(product.id);
    });
    const posts = await this.homeRepo.getFollowingNewsFeedCare(user.id, productIds, id, user.address);
    for (let i = 0; i < posts.length; i += 1) {
      const product = await this.productRepo.getProductById(posts[i].product_id);
      const category = await this.catRepo.getCategoryById(product.category_id);
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        postShared.images = JSON.parse(postShared.images);
        postShared.category_name = category.name;
        postShared.product_name = product.name;
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i] = await this.formatPost(posts[i]);
        posts[i].category_name = category.name;
        posts[i].product_name = product.name;
        posts[i].post = null;
      }
      const isLike = await this.postLikeRepo.getPostLike(posts[i].id, user.id);
      posts[i].is_like = isLike ? 1 : 0;
    }
    return posts;
  }

  /**
   * getPostsNotFollowing
   * @param user
   * @param params
   * @returns {Promise.<Array>}
   */
  async getPostsNotFollowing(user, params) {
    const category = await this.catRepo.getCategoryById(+params.category);
    if (!category) {
      throw new CategoryNotFoundException();
    }
    let id = params.last_id;
    if (id === 0) {
      id = await this.homeRepo.getLastPost() + 1;
    }
    // get list following shop ids
    const shops = await this.shopFollowRepo.getListIdByUserId(user.id);
    const shopIds = [];
    shops.forEach((shop) => {
      shopIds.push(shop.shop_id);
    });
    // get list following user ids
    const users = await this.userFollowRepo.getListUserIdFollowed(user.id);
    const userIds = [];
    users.forEach((item) => {
      userIds.push(item.user_two_id);
    });
    const productIds = [];
    if (params.category !== 1) {
      const products = await this.productRepo.getListProductByCategory(params.category);
      products.forEach((product) => {
        productIds.push(product.id);
      });
    }
    const posts = await this.homeRepo.getNotFollowingNewsFeed(user.id, shopIds, userIds, productIds, id, user.address);
    for (let i = 0; i < posts.length; i += 1) {
      const product = await this.productRepo.getProductById(posts[i].product_id);
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        postShared.images = JSON.parse(postShared.images);
        postShared.category_name = category.name;
        postShared.product_name = product.name;
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i] = await this.formatPost(posts[i]);
        posts[i].category_name = category.name;
        posts[i].product_name = product.name;
        posts[i].post = null;
      }
      const isLike = await this.postLikeRepo.getPostLike(posts[i].id, user.id);
      posts[i].is_like = isLike ? 1 : 0;
    }
    return posts;
  }

  async getPostsCaringNotFollowing(user, params) {
    let id = params.last_id;
    if (id === 0) {
      id = await this.homeRepo.getLastPost() + 1;
    }
    // get list following shop ids
    const shops = await this.shopFollowRepo.getListIdByUserId(user.id);
    const shopIds = [];
    shops.forEach((shop) => {
      shopIds.push(shop.shop_id);
    });
    // get list following user ids
    const users = await this.userFollowRepo.getListUserIdFollowed(user.id);
    const userIds = [];
    users.forEach((item) => {
      userIds.push(item.user_two_id);
    });
    const productIds = [];
    const categories = JSON.parse(user.cares);
    const products = await this.productRepo.getListProductByCategories(categories);
    products.forEach((product) => {
      productIds.push(product.id);
    });
    const posts = await this.homeRepo.getNotFollowingNewsFeedCare(user.id, shopIds, userIds, productIds, id, user.address);
    for (let i = 0; i < posts.length; i += 1) {
      const product = await this.productRepo.getProductById(posts[i].product_id);
      let category;
      if (product) {
        category = await this.catRepo.getCategoryById(product.category_id);
      }
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        postShared.images = JSON.parse(postShared.images);
        postShared.category_name = category.name;
        postShared.product_name = product.name;
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i] = await this.formatPost(posts[i]);
        posts[i].category_name = category.name;
        posts[i].product_name = product.name;
        posts[i].post = null;
      }
      const isLike = await this.postLikeRepo.getPostLike(posts[i].id, user.id);
      posts[i].is_like = isLike ? 1 : 0;
    }
    return posts;
  }

  /**
   * formatPost
   * @param post
   * @return {Promise.<*>}
   */
  async formatPost(post) {
    try {
      post.start_date = formatDateVN(post.start_date);
      post.end_date = formatDateVN(post.end_date);
      const userPosts = await this.postRepo.getUserTotalPost(post.user_id);
      post.user.posts = userPosts;
      if (post.shop_id) {
        const shop = await this.shopRepo.getShopWithPost(post.shop_id);
        shop.posts = await this.postRepo.getShopTotalPost(post.shop_id);
        post.shop = shop;
      }
    } catch (err) {
      console.log(err);
    }
    return post;
  }
}

module.exports = HomeService;
