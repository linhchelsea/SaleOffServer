'use strict';

const ShopFollowRepository = use('App/Repositories/ShopFollowRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const UserRepository = use('App/Repositories/UserRepository');
const BonusRepository = use('App/Repositories/BonusRepository');
class ShopFollowService {
  constructor () {
    this.shopFollowRepo = new ShopFollowRepository();
    this.shopRepo = new ShopRepository();
    this.userRepo = new UserRepository();
    this.bonusRepo = new BonusRepository();
  }

  /**
   * followShop
   * @param id
   * @param code
   * @param userId
   * @return {Promise.<boolean>}
   */
  async followShop(id, code, userId) {
    const follow = await this.shopFollowRepo.followShop(id, userId);
    if (code) {
      const userBonus = await this.userRepo.getUserByBonus(code);
      const isBonus = await this.bonusRepo.check(userBonus.id, userId, id);
      if (!isBonus) {
        await this.bonusRepo.add(userBonus.id, userId, id);
      }
    }
    return follow;
  }

  /**
   * unfollow shop
   * @param shopId
   * @param userId
   * @return {Promise.<boolean>}
   */
  async unfollowShop(shopId, userId) {
    const follow = await this.shopFollowRepo.unfollowShop(shopId, userId);
    return follow;
  }


  /**
   * getListFollowSearch
   * @param user
   * @return {Promise.<Array>}
   */
  async getListFollowSearch(user) {
    const listShopFollowed = await this.shopFollowRepo.getListFollowByUserId(user.id);
    const followedIds = [];
    for (let i = 0; i < listShopFollowed.length; i += 1) {
      followedIds.push(listShopFollowed[i].shop_id);
    }
    const cares = JSON.parse(user.cares);
    const shops = await this.shopRepo.findShopsToFollow(user.id, user.address, cares, followedIds);
    shops.forEach((shop) => {
      followedIds.push(shop.id);
    });
    const moreShops = await this.shopRepo.getMoreToFollow(user.id, user.address, cares, followedIds);
    moreShops.forEach((shop) => {
      shops.push(shop);
    });
    return shops;
  }
}

module.exports = ShopFollowService;
