'use strict';

const PostRepository = use('App/Repositories/PostRepository');
const PostLikeRepository = use('App/Repositories/PostLikeRepository');
const CategoryRepository = use('App/Repositories/CategoryRepository');
const ProductRepository = use('App/Repositories/ProductRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const EventRepository = use('App/Repositories/EventRepository');
const ShopUserRepository = use('App/Repositories/ShopUserRepository');
const UserRepository = use('App/Repositories/UserRepository');
const PostNotFoundException = use('App/Exceptions/PostNotFoundException');
const ShopNotFoundException = use('App/Exceptions/ShopNotFoundException');
const NotYourPostException = use('App/Exceptions/NotYourPostException');
const NotEmployeeException = use('App/Exceptions/NotEmployeeException');
const UserNotFoundException = use('App/Exceptions/UserNotFoundException');
const CantShareYourPostException = use('App/Exceptions/CantShareYourPostException');
const Event = use('Event');
const { formatDate, formatDateTime } = use('App/Helpers');
class PostService {
  constructor() {
    this.postRepo = new PostRepository();
    this.postLikeRepo = new PostLikeRepository();
    this.productRepo = new ProductRepository();
    this.shopRepo = new ShopRepository();
    this.shopUserRepo = new ShopUserRepository();
    this.userRepo = new UserRepository();
    this.eventRepo = new EventRepository();
    this.catRepo = new CategoryRepository();
  }

  /**
   * create
   * @param user
   * @param data
   * @returns {Promise.<boolean>}
   */
  async create(user, data) {
    const post = await this.postRepo.create(user.id, data);
    if (post && data.shop_id) {
      // push notification to shop followers
      Event.fire('notification:shop_follow_new_post', { post });
    }
    return post;
  }

  /**
   * share
   * @param user
   * @param params
   * @return {Promise.<*>}
   */
  async share(user, params) {
    const post = await this.getPost(params.post_id);
    if (post.user_id === user.id) {
      throw new CantShareYourPostException();
    }
    const create = await this.postRepo.share(user.id, params, post.product_id, post.address);
    return create;
  }

  /**
   * getPost
   * @param id
   * @return {Promise.<*>}
   */
  async getPost(id) {
    let post = await this.postRepo.getPost(id);
    if (post) {
      const product = await this.productRepo.getProductById(post.product_id);
      let category;
      if (product) {
        category = await this.catRepo.getCategoryById(product.category_id);
      }
      if (post.is_event) {
        const event = await this.eventRepo.getEventNewsFeed(post.post_id);
        const shop = await this.shopRepo.getShopInfo(event.shop_id);
        const shopCategory = await this.catRepo.getCategoryById(shop.shop_cat_id);
        event.images = JSON.parse(event.images);
        event.cover = event.images[0];
        delete event.images;
        post.category_name = shopCategory.name;
        post.event = event;
      } else if (post.is_share) {
        let postShared = await this.postRepo.getPost(post.post_id);
        postShared = await this.formatPost(postShared);
        post.category_name = category.name;
        post.product_name = product.name;
        post.post = postShared;
      } else {
        post = await this.formatPost(post);
        post.category_name = category.name;
        post.product_name = product.name;
        post.post = null;
      }
    }
    return post;
  }

  /**
   * likePost
   * @param userId
   * @param postId
   * @return {Promise.<boolean>}
   */
  async likePost(userId, postId) {
    const isLike = await this.postLikeRepo.like(postId, userId);
    // send notification
    Event.fire('notification:user_like_your_post', { userId, postId });
    return isLike;
  }

  /**
   * unlikePost
   * @param userId
   * @param postId
   * @return {Promise.<boolean>}
   */
  async unlikePost(userId, postId) {
    return await this.postLikeRepo.unlike(postId, userId);
  }

  /**
   * getPostSmall
   * @param postId
   * @param address
   * @return {Promise.<null>}
   */
  async getPostSmall(postId, address) {
    const post = await this.postRepo.getPostSmall(postId);
    if (!post) {
      throw new PostNotFoundException();
    }
    const posts = await this.postRepo.getRelativePost(post, address);
    posts.forEach((item) => {
      item.start_date = formatDate(item.start_date);
      item.end_date = formatDate(item.end_date);
    });
    return posts;
  }

  /**
   * getYourPosts
   * @param userId
   * @param page
   * @return {Promise.<*>}
   */
  async getYourPosts(userId, page) {
    const user = await this.userRepo.getUserById(userId);
    if (!user) {
      throw new UserNotFoundException();
    }
    const posts = await this.postRepo.getYourPosts(userId, page);
    const postIds = await this.postLikeRepo.getListPostLikeByUserId(userId);
    for (let i = 0; i < posts.length; i += 1) {
      posts[i] = await this.formatPost(posts[i]);
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i].post = null;
      }
      if (postIds.indexOf(posts[i].id) < 0) {
        posts[i].is_like = 0;
      } else {
        posts[i].is_like = 1;
      }
    }
    return posts;
  }

  /**
   * getUserPosts
   * @param userId
   * @param lastId
   * @param id
   * @return {Promise.<Array>}
   */
  async getUserPosts(userId, lastId, id) {
    if (lastId === 0) {
      lastId = await this.postRepo.getLastPost() + 1;
    }
    const posts = await this.postRepo.getUserPosts(userId, lastId);
    const postIds = await this.postLikeRepo.getListPostLikeByUserId(id);
    for (let i = 0; i < posts.length; i += 1) {
      posts[i] = await this.formatPost(posts[i]);
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        posts[i].post = postShared;
      } else if (posts[i].is_event) {
        const event = await this.eventRepo.getEventNewsFeed(posts[i].post_id);
        event.images = JSON.parse(event.images);
        event.from = formatDateTime(event.from);
        event.to = formatDateTime(event.to);
        event.cover = event.images[0];
        posts[i].event = event;
      } else {
        posts[i].post = null;
      }
      if (postIds.indexOf(posts[i].id) < 0) {
        posts[i].is_like = 0;
      } else {
        posts[i].is_like = 1;
      }
    }
    return posts;
  }

  /**
   * getShopPosts
   * @param page
   * @param shopId
   * @param userId
   * @return {Promise.<Array>}
   */
  async getShopPosts(page, shopId, userId) {
    const shop = await this.shopRepo.getShopInfo(shopId);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    const posts = await this.postRepo.getShopPosts(page, shopId);
    const postIds = await this.postLikeRepo.getListPostLikeByUserId(userId);
    for (let i = 0; i < posts.length; i += 1) {
      if (posts[i].is_share) {
        let postShared = await this.postRepo.getPost(posts[i].post_id);
        postShared = await this.formatPost(postShared);
        posts[i].post = postShared;
      } else {
        posts[i] = await this.formatPost(posts[i]);
        posts[i].post = null;
      }
      if (postIds.indexOf(posts[i].id) < 0) {
        posts[i].is_like = 0;
      } else {
        posts[i].is_like = 1;
      }
    }
    return posts;
  }

  async editYourPost(userId, postId, data) {
    const post = await this.postRepo.getPostById(postId);
    if (!post) {
      throw new PostNotFoundException();
    }
    if (post.user_id !== userId) {
      throw new NotYourPostException();
    }
    const edit = await this.postRepo.editPost(postId, data);
    return edit;
  }

  /**
   * editShopPost
   * @param userId
   * @param postId
   * @param shopId
   * @param data
   * @return {Promise.<boolean>}
   */
  async editShopPost(userId, postId, shopId, data) {
    const shop = await this.shopRepo.getShopInfo(shopId);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    const post = await this.postRepo.getPostByIdAndShopId(postId, shopId);
    if (!post) {
      throw new PostNotFoundException();
    }
    if (post.user_id !== userId && shop.owner_id !== userId) {
      throw new NotYourPostException();
    }
    const shopUser = await this.shopUserRepo.getShopUser(shopId, userId);
    if (!shopUser && shop.owner_id !== userId) {
      throw new NotEmployeeException();
    }
    const edit = await this.postRepo.editPost(postId, data);
    return edit;
  }

  /**
   * deletePost
   * @param id
   * @param userId
   * @return {Promise.<boolean>}
   */
  async deleteYourPost(id, userId) {
    const post = await this.postRepo.getPostById(id);
    if (!post) {
      throw new PostNotFoundException();
    }
    if (post.shop_id) {
      throw new NotYourPostException();
    }
    if (post.user_id !== userId) {
      throw new NotYourPostException();
    }
    const isDelete = await this.postRepo.deletePost(id);
    return isDelete;
  }

  /**
   * deleteShopPost
   * @param id
   * @param shopId
   * @param userId
   * @return {Promise.<boolean>}
   */
  async deleteShopPost(id, shopId, userId) {
    const shop = this.shopRepo.getShopInfo(shopId);
    if (!shop) {
      throw new ShopNotFoundException();
    }
    const post = await this.postRepo.getPostByIdAndShopId(id, shopId);
    if (!post) {
      throw new PostNotFoundException();
    }
    if (shop.owner_id !== userId && post.user_id !== userId) {
      throw new NotYourPostException();
    }
    const isDelete = await this.postRepo.deletePost(id);
    return isDelete;
  }

  /**
   * accept
   * @param id
   * @param adminId
   * @return {Promise.<void>}
   */
  async accept(id, adminId) {
    const post = await this.postRepo.getPostById(id);
    if (!post.is_trust) {
      const user = await this.userRepo.getUserById(post.user_id);
      Event.fire('notification:accept_your_post', { postId: id });
      Event.fire('notification:user_follow_new_post', { user, post });
      await this.postRepo.accept(id, adminId);
    }
  }

  /**
   * reject
   * @param id
   * @returns {Promise.<void>}
   */
  async reject(id) {
    const post = await this.postRepo.getPostById(id);
    if (post.is_trust) {
      Event.fire('notification:reject_your_post', { postId: id });
      await this.postRepo.reject(id);
    }
  }

  /**
   * formatPost
   * @param post
   * @return {Promise.<*>}
   */
  async formatPost(post) {
    post.start_date = formatDate(post.start_date);
    post.end_date = formatDate(post.end_date);
    post.images = JSON.parse(post.images);
    // if (post.product_id) {
    //   post.product = await this.productRepo.getProductWithPost(post.product_id);
    // }
    const userPosts = await this.postRepo.getUserTotalPost(post.user_id);
    post.user.posts = userPosts;
    if (post.shop_id) {
      const shop = await this.shopRepo.getShopWithPost(post.shop_id);
      shop.posts = await this.postRepo.getShopTotalPost(post.shop_id);
      post.shop = shop;
    }
    return post;
  }
}

module.exports = PostService;
