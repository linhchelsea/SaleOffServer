'use strict';

const UserRepository = use('App/Repositories/UserRepository');
const AuthRepository = use('App/Repositories/AuthRepository');
const TokenRepository = use('App/Repositories/TokenRepository');
class AuthService {
  /**
   * constructor
   */
  constructor() {
    this.userRepo = new UserRepository();
    this.authRepo = new AuthRepository();
    this.tokenRepo = new TokenRepository();
  }

  /**
   * Login
   * @param params
   * @returns Promise<Object>
   */
  async login(params, auth) {
    const user = await this.userRepo.verify(params.id_token);
    let userLogin = await this.userRepo.findUserByFirebaseUserId(user.user_id);
    user.bonus_code = await this.userRepo.getNewBonusCode();
    let isFirst = 1;
    if (!userLogin) {
      userLogin = await this.userRepo.createUser(user);
    }
    const totalLogin = await this.tokenRepo.getTotalLogin(userLogin.id);
    if (totalLogin > 0) {
      isFirst = 0;
    }
    const jwt = await auth.generate(userLogin);
    await this.authRepo.createToken(userLogin.id, params, jwt);
    return {
      token: jwt.token,
      is_first: isFirst,
    };
  }

  /**
   * logout
   * @param userId
   * @return {Promise.<boolean>}
   */
  async logout(userId) {
    const isLogout = await this.tokenRepo.revokeToken(userId);
    return isLogout;
  }
}

module.exports = AuthService;
