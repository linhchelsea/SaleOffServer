'use strict';

const Api = use('App/Validators/Api');
class Send extends Api {
  get rules () {
    return {
      type: 'required|integer',
      reason: 'required|integer',
      target_id: 'required|integer',
    };
  }

  get messages () {
    return {
      'type.required': 'type_is_required',
      'type.integer': 'type_is_invalid',
      'reason.required': 'reason_is_required',
      'reason.integer': 'reason_is_invalid',
      'target_id.required': 'target_id_is_required',
      'target_id.integer': 'target_id_is_invalid',
    };
  }
}

module.exports = Send;
