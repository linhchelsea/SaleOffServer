'use strict';

const Api = use('App/Validators/Api');
const PostRepository = use('App/Repositories/PostRepository');
const Config = use('Config');
const { postNotFound } = Config.get('error');
class Share extends Api {
  constructor() {
    super();
    this.postRepo = new PostRepository();
  }
  async authorize() {
    const data = this.ctx.request.only(['post_id']);
    const post = await this.postRepo.getPostSmall(data.post_id);
    if (!post) {
      this.authorizeFails(postNotFound);
      return false;
    }
    return true;
  }

  get rules () {
    return {
      description: 'min:6',
      post_id: 'required|integer',
    };
  }

  get messages () {
    return {
      'description.min': 'description_is_too_short',
      'post_id.required': 'post_id_is_required',
      'post_id.integer': 'post_id_is_invalid',
    };
  }
}

module.exports = Share;
