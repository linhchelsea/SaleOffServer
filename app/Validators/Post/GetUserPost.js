'use strict';

const Api = use('App/Validators/Api');
class Get extends Api {
  get rules () {
    return {
      last_id: 'integer',
    };
  }

  get messages () {
    return {
      'last_id.integer': 'last_id_is_invalid',
    };
  }
}

module.exports = Get;
