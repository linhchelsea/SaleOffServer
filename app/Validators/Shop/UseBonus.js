'use strict';

const Api = use('App/Validators/Api');
class UseBonus extends Api {
  get rules () {
    return {
      user_id: 'required|integer',
      point: 'required|integer',
    };
  }

  get messages () {
    return {
      'user_id.required': 'user_id_is_required',
      'user_id.integer': 'user_id_is_invalid',
      'point.required': 'point_is_required',
      'point.integer': 'point_is_invalid',
    };
  }
}

module.exports = UseBonus;
