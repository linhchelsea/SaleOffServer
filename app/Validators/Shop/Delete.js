'use strict';

const Api = use('App/Validators/Api');
const ShopRepository = use('App/Repositories/ShopRepository');
const Config = use('Config');
const { shopNotFound, shopOwner } = Config.get('error');
class Delete extends Api {
  constructor () {
    super();
    this.shopRepo = new ShopRepository();
  }

  async authorize () {
    const userId = await +this.ctx.auth.user.id;
    const { id } = this.ctx.params;
    const shop = await this.shopRepo.getShopInfo(id);
    if (!shop) {
      this.authorizeFails(shopNotFound);
      return false;
    }
    if (shop.owner_id !== userId) {
      this.authorizeFails(shopOwner);
      return false;
    }
    return true;
  }
}

module.exports = Delete;
