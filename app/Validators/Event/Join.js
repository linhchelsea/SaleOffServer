'use strict';

const Api = use('App/Validators/Api');
const JoinEventRepository = use('App/Repositories/JoinEventRepository');
const Config = use('Config');
const { shareCodeNotFound } = Config.get('error');
class Create extends Api {
  constructor() {
    super();
    this.joinEventRepo = new JoinEventRepository();
  }
  async authorize() {
    const params = await this.ctx.request.only(['share_code']);
    console.log(params);
    if (params.share_code) {
      const joinEvent = await this.joinEventRepo.getJoinEventByShareCode(params.share_code);
      if (!joinEvent) {
        this.authorizeFails(shareCodeNotFound);
        return false;
      }
    }
    return true;
  }
  get rules () {
    return {
      event_id: 'required|integer',
      share_code: 'integer',
    };
  }

  get messages () {
    return {
      'event_id.required': 'event_id_is_required',
      'event_id.integer': 'event_id_is_not_a_number',
      'share_code.integer': 'share_code_is_not_a_number',
    };
  }
}

module.exports = Create;
