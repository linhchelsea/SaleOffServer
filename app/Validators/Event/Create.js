'use strict';

const Api = use('App/Validators/Api');
class Create extends Api {
  get rules () {
    return {
      shop_id: 'required|integer',
      name: 'required|min:6',
      detail: 'required|min:6',
      images: 'required|array',
      from: 'required|date',
      to: 'required|date',
      address: 'required|min:6',
    };
  }

  get messages () {
    return {
      'name.required': 'title_is_required',
      'name.min': 'title_is_too_short',
      'detail.required': 'detail_is_required',
      'detail.min': 'detail_is_too_short',
      'images.required': 'images_is_required',
      'images.array': 'images_is_not_array',
      'from.required': 'start_date_is_required',
      'from.date': 'start_date_is_invalid',
      'to.required': 'end_date_is_required',
      'to.date': 'end_date_is_invalid',
      'end_date.required': 'end_date_is_required',
      'end_date.date': 'end_date_is_invalid',
      'address.required': 'address_is_required',
      'address.min': 'address_is_too_short',
    };
  }
}

module.exports = Create;
