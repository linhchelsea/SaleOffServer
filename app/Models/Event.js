'use strict';

const Model = use('Model');

class Event extends Model {
  shop() {
    return this.belongsTo('App/Models/Shop');
  }
}

module.exports = Event;
