'use strict';

const Model = use('Model');

class Admin extends Model {

  static boot() {
    super.boot();
    this.addHook('beforeCreate', 'Admin.hashPassword');
  }
  /**
   * Hiding Fields
   * @returns {string[]}
   */
  static get hidden () {
    return ['password'];
  }

  role () {
    return this.hasOne('App/Models/Role');
  }
}

module.exports = Admin;
