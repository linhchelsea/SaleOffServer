'use strict';

const Model = use('Model');

class JoinEvent extends Model {
  static get table () {
    return 'join_events';
  }
  user () {
    return this.belongsTo('App/Models/User');
  }
}

module.exports = JoinEvent;
