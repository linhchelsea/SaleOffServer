'use strict';

const Model = use('Model');

class Bonus extends Model {
  user () {
    return this.belongsTo('App/Models/User', 'user_one_id', 'id');
  }
  receiver () {
    return this.belongsTo('App/Models/User', 'user_two_id', 'id');
  }
}

module.exports = Bonus;
