/**
 * @api {GET} http://localhost:3333/api/v1/report/:type/get Get list report reason
 * @apiName Get list report reason
 * @apiGroup Report
 *
 * @apiParam {Integer} type Report type (1: post, 2: user, 3: shop)
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "reposts": {
 *           "1": "Cửa hàng này là giả mạo",
 *           "2": "Cửa hàng kinh doanh hàng cấm",
 *           "3": "Cửa hàng thường xuyên đăng tải thông tin không lành mạnh",
 *           "4": "Cửa hàng có hành vi lừa đảo",
 *           "5": "Cửa hàng có những bình luận xuyên tạc"
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "reposts": {
 *           "1": "Người dùng này là giả mạo",
 *           "2": "Người dùng có hành vi quấy rối",
 *           "3": "Người dùng thường xuyên đăng tải thông tin không lành mạnh",
 *           "4": "Người dùng có hành vi lừa đảo",
 *           "5": "Người dùng có những bình luận xuyên tạc"
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "reposts": {
 *            "1": "Bài viết chứa thông tin sai sự thật",
 *           "2": "Bài viết chứa nội dung không lành mạnh",
 *           "3": "Bài viết chứa nội dung xuyên tạc, chống phá nhà nước",
 *           "4": "Bài viết chứa những bình luận mang tính tiêu cực"
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */

/**
 * @api {POST} http://localhost:3333/api/v1/report/send Send report
 * @apiName Send report
 * @apiGroup Report
 *
 * @apiParam {Integer} type Report type (1: post, 2: user, 3: shop)
 * @apiParam {Integer} reason Report reason
 * @apiParam {Integer} target_id Target Id (postID, userID, shopID)
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 * }
 */
