/**
 * @api {POST} http://localhost:3333/api/v1/post/create Create new post
 * @apiName Create new post
 * @apiGroup Post
 *
 * @apiParam {String} title Title
 * @apiParam {String} description Description
 * @apiParam {Integer} sale_percent Sale percent [0,100]
 * @apiParam {URL} cover Cover image
 * @apiParam {Array[url]} images list images(5 images)
 * @apiParam {Date} start_date Start date
 * @apiParam {Date} end_date End date
 * @apiParam {String} address Address
 * @apiParam {Integer} product_id Product ID
 * @apiParam {Integer} shop_id Shop ID
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "post": {
 *           "title": "Bai viet so 3",
 *           "description": "Mo ra so 3",
 *           "sale_percent": 50,
 *           "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *           "images": "[\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\"]",
 *           "start_date": "2018-03-22",
 *           "end_date": "2018-03-25",
 *           "address": "81 Quang Trung",
 *           "product_id": 1,
 *           "shop_id": null,
 *           "is_trust": false,
 *           "user_id": 4,
 *           "is_share": 0,
 *           "created_at": "2018-04-26 16:59:02",
 *           "updated_at": "2018-04-26 16:59:02",
 *           "id": 43,
 *           "post_id": null,
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 *
 * @apiError ValidationError Somethings are wrong
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "title_is_required",
 *   "error": 400
 *}
 * @apiError System Error Connection is bad
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 * @apiError Product not found
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "product_not_found",
 *   "error": 19
 *}
 * @apiError Not shop member
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_shop_member",
 *   "error": 21
 *}
 */
/**
 * @api {POST} http://localhost:3333/api/v1/post/share Share post
 * @apiName Share post
 * @apiGroup Post
 *
 * @apiParam {String} description Description
 * @apiParam {Integer} post_id Post ID
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 *
 * @apiError ValidationError Somethings are wrong
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "title_is_required",
 *   "error": 400
 *}
 * @apiError System Error Connection is bad
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 * @apiError Product not found
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "product_not_found",
 *   "error": 19
 *}
 * @apiError Not shop member
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_shop_member",
 *   "error": 21
 *}
 */
/**
 * @api {GET} http://localhost:3333/api/v1/post/:id/get Get post detail
 * @apiName Get post detail
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 *
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "post": {
 *           "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *           "description": "adsdasdasda",
 *           "sale_percent": 0,
 *           "cover": null,
 *           "images": [],
 *           "start_date": null,
 *           "end_date": null,
 *           "address": null,
 *           "product_id": null,
 *           "shop_id": null,
 *           "is_trust": 1,
 *           "user_id": 2,
 *           "is_share": 1,
 *           "created_at": "2018-04-26 16:59:02",
 *           "updated_at": "2018-04-26 16:59:02",
 *           "id": 49,
 *           "post": {
 *               "id": 48,
 *               "title": "tao la linh",
 *               "description": "linh la tao",
 *               "sale_percent": 11,
 *               "is_trust": 1,
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *               "images": [
 *                   "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                   "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                   "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                   "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                   "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               ],
 *               "start_date": "2018-04-24",
 *               "end_date": "2018-04-24",
 *               "address": "81 Quang Trung",
 *               "view": 1,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": 5,
 *               "shop_id": 1,
 *               "user_id": 4,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-05-08 00:12:52",
 *               "updated_at": "2018-05-14 23:13:06",
 *               "is_share": 0,
 *               "category_name": "Sách, tạp chí",
 *               "product_name": "Sách kỹ năng sống",
 *               "post_id": null,
 *               "user": {
 *                   "id": 4,
 *                   "username": "linhchelsea123",
 *                   "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                   "follows": 1,
 *                   "posts": 5
 *               },
 *               "product": {
 *                   "id": 5,
 *                   "name": "Lẩu và nướng"
 *               },
 *               "shop": {
 *                   "id": 1,
 *                   "name": "Fm Style",
 *                   "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                   "rate": 0,
 *                   "follows": 1,
 *                   "posts": 13
 *               }
 *           }
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "post": {
 *           "id": 63,
 *           "title": null,
 *           "description": "sdjfsuydgfusydtfuysfusdfsdtf",
 *           "sale_percent": 0,
 *           "is_trust": 1,
 *           "cover": null,
 *           "images": "[]",
 *           "start_date": null,
 *           "end_date": null,
 *           "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *           "view": 6,
 *           "like": 0,
 *           "comment": 0,
 *           "product_id": null,
 *           "shop_id": null,
 *           "user_id": 2,
 *           "admin_id": null,
 *           "is_checked": 0,
 *           "created_at": "2018-06-01 22:11:46",
 *           "updated_at": "2018-06-02 00:20:38",
 *           "is_share": 0,
 *           "post_id": 10,
 *           "is_event": 1,
 *           "user": {
 *               "id": 2,
 *               "username": "nguyen manh linh",
 *               "avatar": "https://images.unsplash.com/photo-1496072298559-ee7eacbd1b39?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f7187f42114410b39d3fb69541267b13&auto=format&fit=crop&w=1050&q=80",
 *               "follows": 0
 *           },
 *           "category_name": "Thể thao",
 *           "event": {
 *               "id": 10,
 *               "name": "Giảm giá nhân ngày khai trương",
 *               "detail": "Đây là chi tiết của khuyến mãi",
 *               "shop_id": 10,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "status": 1,
 *               "created_at": "2018-05-29 22:51:11",
 *               "updated_at": "2018-06-01 22:11:46",
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *           }
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError Post not found
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 */
/**
 * @api {GET} http://localhost:3333/api/v1/post/:id/like Like post
 * @apiName Like post
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError PostLiked Post liked
 * @apiErrorExample Post liked:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_liked",
 *   "error": 23
 *}
 */
/**
 * @api {GET} http://localhost:3333/api/v1/post/:id/unlike Unlike post
 * @apiName Unlike post
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError PostUnliked Post unliked
 * @apiErrorExample Post unliked:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_unliked",
 *   "error": 24
 *}
 */

/**
 * @api {GET} http://localhost:3333/api/v1/post/:id/relative Get relative posts
 * @apiName Get relative posts
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "posts": [
 *            {
 *               "id": 17,
 *               "title": "Corrupti aliquam reprehenderit voluptate earum enim anim accusantium exercitationem sit voluptatem cupiditate earum soluta est odit sunt culpa",
 *               "description": "Qui enim reprehenderit amet ipsam quia voluptate ipsam sit quae incidunt consequatur fugit illo ullam veniam in nihil reiciendis quodQui enim reprehenderit amet ipsam quia voluptate ipsam sit quae incidunt consequatur fugit illo ullam veniam in nihil reiciendis quodQui enim reprehenderit amet ipsam quia voluptate ipsam sit quae incidunt consequatur fugit illo ullam veniam in nihil reiciendis quod",
 *               "sale_percent": 27,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "images": "[\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\"]",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "Ea officia est et error explicabo Aut est quia rerum fugiat vel eveniet corporis quam sit cupidatat",
 *               "view": 2165,
 *               "like": 406,
 *               "comment": 0,
 *               "product_id": 46,
 *               "shop_id": null,
 *               "user_id": 3,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-04-11 15:25:41",
 *               "updated_at": "2018-04-11 15:25:41"
 *           },
 *           {
 *               "id": 1,
 *               "title": "Laboris accusamus voluptas eaque consequat Expedita amet sit quo temporibus aut est",
 *               "description": "Dolorum in sit dignissimos aliqua Tempora quam laboris odio sit accusamus",
 *               "sale_percent": 39,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "images": "[\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\",\"https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5\"]",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "Non ut quo enim rerum elit eos harum aut quod mollit",
 *               "view": 3523,
 *               "like": 448,
 *               "comment": 0,
 *               "product_id": 46,
 *               "shop_id": 1,
 *               "user_id": 5,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-04-11 15:25:31",
 *               "updated_at": "2018-04-11 15:25:31"
 *           },
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 */

/**
 * @api {GET} http://localhost:3333/api/v1/post/:page/yours Get your posts
 * @apiName Get your posts
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "posts": [
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *           {
 *               "id": 62,
 *               "title": null,
 *               "description": "sdjfsuydgfusydtfuysfusdfsdtf",
 *               "sale_percent": 0,
 *               "is_trust": 1,
 *               "cover": null,
 *               "images": "[]",
 *               "start_date": null,
 *               "end_date": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "view": 0,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "shop_id": null,
 *               "user_id": 7,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-05-27 15:33:43",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "is_share": 0,
 *               "post_id": 1,
 *               "is_event": 1,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg",
 *                   "follows": 0
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "view": 17,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "is_like": 0
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 */

/**
 * @api {GET} http://localhost:3333/api/v1/post/:page/shop/:shopId Get shop posts
 * @apiName Get shop posts
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "posts": [
 *           {
 *               "id": 2,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "view": 0,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": 1,
 *               "shop_id": 1,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:51:04",
 *               "updated_at": "2018-03-24 15:51:04",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "user": {
 *                   "id": 2,
 *                   "user_id": "X209h95Jg8PzPU09L9cGGrJKKo12",
 *                   "email": "linhchelseatoeic95@gmail.com",
 *                   "username": "Linh Nguyen",
 *                   "full_name": "Linh Nguyen",
 *                   "address": null,
 *                   "avatar": "https://lh5.googleusercontent.com/-Z1JD_dstN60/AAAAAAAAAAI/AAAAAAAAAAs/2ybLEfIooPE/s96-c/photo.jpg",
 *                   "gender": 0,
 *                   "birthday": "2017-12-31T17:00:00.000Z",
 *                   "phone": null,
 *                   "is_notify": 1,
 *                   "created_at": "2018-03-19 20:00:09",
 *                   "updated_at": "2018-03-28 23:17:16",
 *                   "follows": 0
 *               },
 *               "is_like": 0,
 *           },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "view": 17,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": 1,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "user": {
 *                   "id": 2,
 *                   "user_id": "X209h95Jg8PzPU09L9cGGrJKKo12",
 *                   "email": "linhchelseatoeic95@gmail.com",
 *                   "username": "Linh Nguyen",
 *                   "full_name": "Linh Nguyen",
 *                   "address": null,
 *                   "avatar": "https://lh5.googleusercontent.com/-Z1JD_dstN60/AAAAAAAAAAI/AAAAAAAAAAs/2ybLEfIooPE/s96-c/photo.jpg",
 *                   "gender": 0,
 *                   "birthday": "2017-12-31T17:00:00.000Z",
 *                   "phone": null,
 *                   "is_notify": 1,
 *                   "created_at": "2018-03-19 20:00:09",
 *                   "updated_at": "2018-03-28 23:17:16",
 *                   "follows": 0
 *               },
 *               "is_like": 1,
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 */
/**
 * @api {PUT} http://localhost:3333/api/v1/post/:id/edit-your-post Edit Your Post
 * @apiName Edit Your Post
 * @apiGroup Post
 *
 * @apiParam {String} title Title [optional]
 * @apiParam {String} description Description [optional]
 * @apiParam {Integer} sale_percent Sale percent [0,100] [optional]
 * @apiParam {URL} cover Cover image [optional]
 * @apiParam {Array[url]} images list images(5 images) [optional]
 * @apiParam {Date} start_date Start date [optional]
 * @apiParam {Date} end_date End date [optional]
 * @apiParam {String} address Address [optional]
 * @apiParam {Integer} product_id Product ID [optional]
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError NotYourPost Not your post
 * @apiErrorExample Not your post
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_your_post",
 *   "error": 27
 *}
 */
/**
 * @api {PUT} http://localhost:3333/api/v1/post/:id/edit/:shopId/shop Edit shop Post
 * @apiName Edit shop Post
 * @apiGroup Post
 *
 * @apiParam {String} title Title [optional]
 * @apiParam {String} description Description [optional]
 * @apiParam {Integer} sale_percent Sale percent [0,100] [optional]
 * @apiParam {URL} cover Cover image [optional]
 * @apiParam {Array[url]} images list images(5 images) [optional]
 * @apiParam {Date} start_date Start date [optional]
 * @apiParam {Date} end_date End date [optional]
 * @apiParam {String} address Address [optional]
 * @apiParam {Integer} product_id Product ID [optional]
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError NotYourPost Not your post
 * @apiErrorExample Not your post
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_your_post",
 *   "error": 27
 *}
 * @apiError ShopNotFound Shop not found
 * @apiErrorExample Shop not found
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 * @apiError NotShopMember Not shop member
 * @apiErrorExample Not shop member
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_shop_member",
 *   "error": 21
 *}
 */
/**
 * @api {DELETE} http://localhost:3333/api/v1/post/:id/delete-your-post Delete your post
 * @apiName Delete your post
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError NotYourPost Not your post
 * @apiErrorExample Not your post
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_your_post",
 *   "error": 27
 *}
 */
/**
 * @api {DELETE} http://localhost:3333/api/v1/post/:id/delete/:shopId/shop Delete shop post
 * @apiName Delete shop post
 * @apiGroup Post
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 * @apiError NotYourPost Not your post
 * @apiErrorExample Not your post
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_your_post",
 *   "error": 27
 *}
 * @apiError ShopNotFound Shop Not Found
 * @apiErrorExample Shop Not Found
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 */

/**
 * @api {POST} http://localhost:3333/api/v1/post/:userId/user Get user posts
 * @apiName Get user posts
 * @apiGroup Post
 *
 * @apiParam {Integer} lastId Last ID. <br/>Default: <code>0</code>
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "posts": [
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *               "id": 62,
 *               "title": null,
 *               "description": "sdjfsuydgfusydtfuysfusdfsdtf",
 *               "sale_percent": 0,
 *               "is_trust": 1,
 *               "cover": null,
 *               "images": "[]",
 *               "start_date": null,
 *               "end_date": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "view": 0,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "shop_id": null,
 *               "user_id": 7,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-05-27 15:33:43",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "is_share": 0,
 *               "post_id": 1,
 *               "is_event": 1,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg",
 *                   "follows": 0
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "view": 17,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "is_like": 0
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 *}
 * @apiError PostNotFound Post not found
 * @apiErrorExample Post not found:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "post_not_found",
 *   "error": 22
 *}
 */
