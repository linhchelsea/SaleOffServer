/**
 * @api {GET} http://localhost:3333/api/v1/notification/get Get list notification
 * @apiName Get list notification
 * @apiGroup Notification
 *
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "notifications": [
 *           {
 *               "body": {
 *                   "avatar": "https://lh6.googleusercontent.com/-6vKlFbVspzs/AAAAAAAAAAI/AAAAAAAAAPA/VZXOFh4hrvI/s96-c/photo.jpg",
 *                   "content": "Linh Nguyễn vừa thêm một bài viết mới"
 *               },
 *               "target": {
 *                   "id": 43,
 *                   "type": "post"
 *               },
 *               "created_at": "2018-04-26 16:59:02",
 *               "sender_id": 0,
 *               "type_sender": "user",
 *               "id": "AVLnRnzmYEEmqDtWWs6Q"
 *           },
 *           {
 *               "body": {
 *                   "content": "Chủ cửa hàng Quán thịt chó Ahihi đã xóa bạn khỏi danh sách nhân viên",
 *                   "avatar": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29261911_500147840383143_4317207633091100672_n.jpg?_nc_cat=0&oh=597a9b97c67dd86eb607110f6c3fe657&oe=5B327FC3"
 *               },
 *               "target": {
 *                   "type": "shop",
 *                   "id": 6
 *               },
 *               "created_at": "2018-04-25 14:08:48",
 *               "sender_id": 6,
 *               "type_sender": "shop",
 *               "id": "m99E3GyZGTzafqtfvsQI"
 *           },
 *           {
 *               "body": {
 *                   "content": "Chủ cửa hàng Quán thịt chó Ahihi đã xóa bạn khỏi danh sách nhân viên",
 *                   "avatar": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29261911_500147840383143_4317207633091100672_n.jpg?_nc_cat=0&oh=597a9b97c67dd86eb607110f6c3fe657&oe=5B327FC3"
 *               },
 *               "target": {
 *                   "id": 6,
 *                   "type": "shop"
 *               },
 *               "created_at": "2018-04-25 14:08:41",
 *               "sender_id": 6,
 *               "type_sender": "shop",
 *               "id": "pxwotTJ3BPwJDv5ZjH6b"
 *           },
 *           {
 *               "body": {
 *                   "content": "Chủ cửa hàng Quán thịt chó Ahihi đã xóa bạn khỏi danh sách nhân viên",
 *                   "avatar": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29261911_500147840383143_4317207633091100672_n.jpg?_nc_cat=0&oh=597a9b97c67dd86eb607110f6c3fe657&oe=5B327FC3"
 *               },
 *               "target": {
 *                   "type": "shop",
 *                   "id": 6
 *               },
 *               "created_at": "2018-04-25 14:05:41",
 *               "sender_id": "6",
 *               "type_sender": "shop",
 *               "id": "RxyDz6vi1jZZHlKHbP86"
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */
/**
 * @api {DELETE} http://localhost:3333/api/v1/notification/:id/delete Delete notification by id
 * @apiName Delete notification by id
 * @apiGroup Notification
 *
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 * }
 */
