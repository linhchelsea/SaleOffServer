/**
 * @api {POST} http://localhost:3333/api/v1/events/create Create new event
 * @apiName Create new event
 * @apiGroup Event
 *
 * @apiParam {Integer} shop_id Shop Id
 * @apiParam {String} name Event name
 * @apiParam {String} detail Event detail
 * @apiParam {String} address Event address
 * @apiParam {Array[String]} images Event images
 * @apiParam {DateTime} from begin time
 * @apiParam {DateTime} to end time
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 * }
 *
 * @apiError ValidationError Somethings are wrong
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "not_shop_owner",
 *   "error": 6
 *}
 * @apiError System Error Connection is bad
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "shop_not_found",
 *   "error": 5
 *}
 * @apiError Shop category not found
 * @apiErrorExample Error-Response:
 *{
 *   "status": 401,
 *   "data": null,
 *   "message": "begin_time_is_over",
 *   "error": 38
 *}
 */
/**
 * @api {GET} http://localhost:3333/api/v1/events/:id/detail Get Event detail
 * @apiName Get Event detail
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "event": {
 *           "id": 1,
 *           "shop_id": 4,
 *           "total": 0,
 *           "name": "Giảm giá nhân ngày khai trương",
 *           "detail": "Đây là chi tiết của khuyến mãi",
 *           "images": "[\"https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png\",\"https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png\",\"https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png\",\"https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png\",\"https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png\"]",
 *           "from": "2018-05-24T15:00:00.000Z",
 *           "to": "2018-05-26T15:00:00.000Z",
 *           "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *           "status": 1,
 *           "created_at": "2018-05-24 23:28:13",
 *           "updated_at": "2018-05-24 23:28:13",
 *           "shop": {
 *               "id": 6,
 *               "name": "Shop 0000000",
 *               "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *           },
 *           "is_join": 0,
 *           "share_code": 0,
 *       }
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */
/**
 * @api {POST} http://localhost:3333/api/v1/events/join Join Event
 * @apiName Join Event
 * @apiGroup Event
 *
 * @apiParam {Integer} event_id Event ID
 * @apiParam {Integer} share_code Share Code
 * @apiParam {String} description Post description
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "share_code": 100006
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */

/**
 * @api {GET} http://localhost:3333/api/v1/events/:id/participants Get list participants
 * @apiName Get list participants
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "participants": [
 *           {
 *               "id": 2,
 *               "avatar": "https://images.unsplash.com/photo-1496072298559-ee7eacbd1b39?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f7187f42114410b39d3fb69541267b13&auto=format&fit=crop&w=1050&q=80",
 *               "username": "nguyen manh linh",
 *               "share_code": 100001,
 *               "point": 1,
 *               "is_used": 0,
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */
/**
 * @api {GET} http://localhost:3333/api/v1/events/:shopId/shop Get list shop events
 * @apiName Get list shop events
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *
 *       "events": [
 *           {
 *               "id": 1,
 *               "name": "Giảm giá nhân ngày khai trương",
 *               "detail": "Đây là chi tiết của khuyến mãi",
 *               "shop_id": 6,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "status": 1,
 *               "created_at": "2018-05-24 23:28:13",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "shop": {
 *                   "id": 6,
 *                   "name": "Shop 0000000",
 *                   "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *               },
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */

/**
 * @api {GET} http://localhost:3333/api/v1/events Get list events you joined
 * @apiName Get list events you joined
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "events": [
 *           {
 *               "id": 1,
 *               "name": "Giảm giá nhân ngày khai trương",
 *               "detail": "Đây là chi tiết của khuyến mãi",
 *               "shop_id": 6,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "status": 1,
 *               "created_at": "2018-05-24 23:28:13",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "shop": {
 *                   "id": 6,
 *                   "name": "Shop 0000000",
 *                   "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *               },
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */

/**
 * @api {GET} http://localhost:3333/api/v1/events/:id/user/:userId/use Use event point
 * @apiName Use event point
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": null,
 *   "message": "success",
 *   "error": 0
 * }
 */

/**
 * @api {GET} http://localhost:3333/api/v1/events/:page/recently/:type Get list recently events
 * @apiName Get list recently events
 * @apiGroup Event
 *
 * @apiParam {Integer} page Page
 * @apiParam {Integer} type Type of events. <br/><code>1: will be happened</code><br/><code>2: happening</code><br/><code>3: happened</code>
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *       "events": [
 *           {
 *               "id": 1,
 *               "name": "Giảm giá nhân ngày khai trương",
 *               "detail": "Đây là chi tiết của khuyến mãi",
 *               "shop_id": 6,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "status": 1,
 *               "created_at": "2018-05-24 23:28:13",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "shop": {
 *                   "id": 6,
 *                   "name": "Shop 0000000",
 *                   "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *               },
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *               "is_joined": 0
 *           },
 *           {
 *              "id": 1,
 *               "shop_id": 6,
 *               "total": 1,
 *               "name": "Giảm giá nhân ngày khai trương",
 *               "detail": "Đây là chi tiết của khuyến mãi",
 *               "from": "2018-05-24T15:00:00.000Z",
 *               "to": "2018-05-26T15:00:00.000Z",
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "status": 1,
 *               "created_at": "2018-05-24 23:28:13",
 *               "updated_at": "2018-05-27 15:33:43",
 *               "shop": {
 *                   "id": 6,
 *                   "name": "Shop 0000000",
 *                   "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *               },
 *               "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *               "is_joined": 1
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */
/**
 * @api {POST} http://localhost:3333/api/v1/events/:id/search-participants Search participants by username
 * @apiName Search participants by username
 * @apiGroup Event
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *      "participants": [
 *           {
 *               "id": 1,
 *               "user_id": "tSn6AP1CKphkvvq3iINAdiM82Hv2",
 *               "email": null,
 *               "username": "Linh Chelsea",
 *               "full_name": "Linh Chelsea",
 *               "address": "81 quang trung",
 *               "avatar": "https://www.petxinh.net/wp-content/uploads/2017/03/maxresdefault.jpg",
 *               "gender": 1,
 *               "birthday": "1995-10-20T17:00:00.000Z",
 *               "phone": "0935679844",
 *               "is_notify": 1,
 *               "created_at": "2018-04-10 16:38:46",
 *               "updated_at": "2018-04-11 10:16:40",
 *               "follows": 1,
 *               "cover": "https://images.unsplash.com/photo-1496072298559-ee7eacbd1b39?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f7187f42114410b39d3fb69541267b13&auto=format&fit=crop&w=1050&q=80",
 *               "cares": [],
 *               "bonus_code": 100001,
 *               "is_block": 0,
 *               "share_code": 100001
 *           }
 *       ]
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 */
