/**
 * @api {POST} http://localhost:3333/api/v1/newsfeed/following Following Newsfeed
 * @apiName Following Newsfeed
 * @apiGroup Newsfeed
 *
 * @apiParam {Integer} last_id Post ID. </br><code>Default: 0</code>
 * @apiParam {Integer} category Category ID
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *        "posts": [
 *          {
 *               "id": 62,
 *               "title": null,
 *               "sale_percent": 0,
 *               "start_date": null,
 *               "end_date": null,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "created_at": "2018-05-27 15:33:43",
 *               "shop_id": null,
 *               "user_id": 7,
 *               "cover": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "is_share": 0,
 *               "is_event": 1,
 *               "post_id": 1,
 *               "shop": null,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg"
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *               "is_like": 0
 *           },
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "is_share": 0,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "post": null,
 *               "is_like": 0
 *           }
 *       ],
 *       "last_id": 7,
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 *
 * @apiError LastIdInvalid Last Id is not a number
 * @apiErrorExample Error-Response:
 *{
 *   "status": 400,
 *   "data": null,
 *   "message": "last_id_is_invalid",
 *   "error": 400
 *}
 */
/**
 * @api {POST} http://localhost:3333/api/v1/newsfeed/not-following Not Following Newsfeed
 * @apiName Not Following Newsfeed
 * @apiGroup Newsfeed
 *
 * @apiParam {Integer} last_id Post ID. </br><code>Default: 0</code>
 * @apiParam {Integer} category Category ID
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *        "posts": [
 *           {
 *               "id": 62,
 *               "title": null,
 *               "sale_percent": 0,
 *               "start_date": null,
 *               "end_date": null,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "created_at": "2018-05-27 15:33:43",
 *               "shop_id": null,
 *               "user_id": 7,
 *               "cover": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "is_share": 0,
 *               "is_event": 1,
 *               "post_id": 1,
 *               "shop": null,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg"
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *               "is_like": 0
 *           },
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "is_share": 0,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "post": null,
 *               "is_like": 0
 *           }
 *       ],
 *       "last_id": 7,
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 *
 * @apiError LastIdInvalid Last Id is not a number
 * @apiErrorExample Error-Response:
 *{
 *   "status": 400,
 *   "data": null,
 *   "message": "last_id_is_invalid",
 *   "error": 400
 *}
 */

/**
 * @api {POST} http://localhost:3333/api/v1/newsfeed/care-follow Care Following Newsfeed
 * @apiName Care Following Newsfeed
 * @apiGroup Newsfeed
 *
 * @apiParam {Integer} last_id Post ID. </br><code>Default: 0</code>
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *        "posts": [
 *          {
 *               "id": 62,
 *               "title": null,
 *               "sale_percent": 0,
 *               "start_date": null,
 *               "end_date": null,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "created_at": "2018-05-27 15:33:43",
 *               "shop_id": null,
 *               "user_id": 7,
 *               "cover": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "is_share": 0,
 *               "is_event": 1,
 *               "post_id": 1,
 *               "shop": null,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg"
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *               "is_like": 0
 *           },
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "is_share": 0,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "post": null,
 *               "is_like": 0
 *           }
 *       ],
 *       "last_id": 7,
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 *
 * @apiError LastIdInvalid Last Id is not a number
 * @apiErrorExample Error-Response:
 *{
 *   "status": 400,
 *   "data": null,
 *   "message": "last_id_is_invalid",
 *   "error": 400
 *}
 */
/**
 * @api {POST} http://localhost:3333/api/v1/newsfeed/care-not-follow Care Not Following Newsfeed
 * @apiName Care Not Following Newsfeed
 * @apiGroup Newsfeed
 *
 * @apiParam {Integer} last_id Post ID. </br><code>Default: 0</code>
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 * {
 *   "status": 200,
 *   "data": {
 *        "posts": [
 *          {
 *               "id": 62,
 *               "title": null,
 *               "sale_percent": 0,
 *               "start_date": null,
 *               "end_date": null,
 *               "like": 0,
 *               "comment": 0,
 *               "product_id": null,
 *               "created_at": "2018-05-27 15:33:43",
 *               "shop_id": null,
 *               "user_id": 7,
 *               "cover": null,
 *               "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *               "is_share": 0,
 *               "is_event": 1,
 *               "post_id": 1,
 *               "shop": null,
 *               "user": {
 *                   "id": 7,
 *                   "username": "LAI TRAN",
 *                   "avatar": "https://lh5.googleusercontent.com/-0xCD07m7XsQ/AAAAAAAAAAI/AAAAAAAAAFI/9vUkutq4I4g/s96-c/photo.jpg"
 *               },
 *               "event": {
 *                   "id": 1,
 *                   "shop_id": 6,
 *                   "total": 1,
 *                   "name": "Giảm giá nhân ngày khai trương",
 *                   "detail": "Đây là chi tiết của khuyến mãi",
 *                   "images": [
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                       "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                   ],
 *                   "from": "2018-05-24T15:00:00.000Z",
 *                   "to": "2018-05-26T15:00:00.000Z",
 *                   "address": "68 Hàm Nghi, Thanh Khê, Đà Nẵng",
 *                   "status": 1,
 *                   "created_at": "2018-05-24 23:28:13",
 *                   "updated_at": "2018-05-27 15:33:43",
 *                   "shop": {
 *                       "id": 6,
 *                       "name": "Shop 0000000",
 *                       "avatar": "https://i-xem.mkocdn.com/i.xem.sb/data/photo/2018/04/27/008/can-ca-nuoc-mat-1524791008-400.jpg"
 *                   },
 *                   "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *               },
 *               "is_like": 0
 *           },
 *          {
 *              "id": 49,
 *              "title": "nguyen manh linh đã chia sẻ bài viết của Fm Style",
 *              "description": "adsdasdasda",
 *              "sale_percent": 0,
 *              "cover": null,
 *              "images": [],
 *              "start_date": null,
 *              "end_date": null,
 *              "address": null,
 *              "product_id": null,
 *              "shop_id": null,
 *              "is_trust": 1,
 *              "user_id": 2,
 *              "is_share": 1,
 *              "created_at": "2018-04-26 16:59:02",
 *              "updated_at": "2018-04-26 16:59:02",
 *              "post": {
 *                  "id": 48,
 *                  "title": "tao la linh",
 *                  "description": "linh la tao",
 *                  "sale_percent": 11,
 *                  "is_trust": 1,
 *                  "cover": "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                  "images": [
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png",
 *                      "https://banbuonsi.vn/wp-content/uploads/2017/12/rubik-cu-be.png"
 *                  ],
 *                  "start_date": "2018-04-24",
 *                  "end_date": "2018-04-24",
 *                  "address": "81 Quang Trung",
 *                  "view": 1,
 *                  "like": 0,
 *                  "comment": 0,
 *                  "product_id": 5,
 *                  "shop_id": 1,
 *                  "user_id": 4,
 *                  "admin_id": null,
 *                  "is_checked": 0,
 *                  "created_at": "2018-05-08 00:12:52",
 *                  "updated_at": "2018-05-14 23:13:06",
 *                  "is_share": 0,
 *                  "post_id": null,
 *                  "user": {
 *                      "id": 4,
 *                      "username": "linhchelsea123",
 *                      "avatar": "https://images.unsplash.com/photo-1522204657746-fccce0824cfd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=84b5e9bea51f72c63862a0544f76e0a3&auto=format&fit=crop&w=1050&q=80",
 *                      "follows": 1,
 *                      "posts": 5
 *                  },
 *                  "product": {
 *                      "id": 5,
 *                      "name": "Lẩu và nướng"
 *                  },
 *                  "shop": {
 *                      "id": 1,
 *                      "name": "Fm Style",
 *                      "avatar": "https://upload.wikimedia.org/wikipedia/vi/thumb/5/5c/Chelsea_crest.svg/1024px-Chelsea_crest.svg.png",
 *                      "rate": 0,
 *                      "follows": 1,
 *                      "posts": 13
 *                  }
 *               },
 *               "is_like": 0
 *           },
 *           {
 *               "id": 1,
 *               "title": "Bai viet so 1",
 *               "description": "Mo ra so 1",
 *               "sale_percent": 33,
 *               "is_trust": 1,
 *               "cover": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *               "start_date": "2018-03-21T17:00:00.000Z",
 *               "end_date": "2018-03-24T17:00:00.000Z",
 *               "address": "81 Quang Trung",
 *               "is_share": 0,
 *               "like": 0,
 *               "comment": 4,
 *               "product_id": 1,
 *               "shop_id": null,
 *               "user_id": 2,
 *               "admin_id": null,
 *               "is_checked": 0,
 *               "created_at": "2018-03-24 15:34:39",
 *               "updated_at": "2018-03-27 22:58:18",
 *               "images": [
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5",
 *                   "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29357003_2081966722058348_3420295775945490432_n.png?_nc_cat=0&_nc_eui2=v1%3AAeGk59hg2P2OCvQjDw9Gx64HxxLiAcrSUKlzdeih9qGVYALlFkO9BI2_Erw9PdN57hivyT_OLgUCFP38D54hT4orO-tfXSUxGTVdEV1yDvGlRA&oh=eed3ea72f9dd2e262321a4ef8caacf72&oe=5B711EC5"
 *               ],
 *               "product": {
 *                   "id": 1,
 *                   "name": "Nước giải khát"
 *               },
 *               "post": null,
 *               "is_like": 0
 *           }
 *       ],
 *       "last_id": 7,
 *   },
 *   "message": "success",
 *   "error": 0
 * }
 *
 * @apiError LastIdInvalid Last Id is not a number
 * @apiErrorExample Error-Response:
 *{
 *   "status": 400,
 *   "data": null,
 *   "message": "last_id_is_invalid",
 *   "error": 400
 *}
 */
