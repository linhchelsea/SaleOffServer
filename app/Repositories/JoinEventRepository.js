'use strict';

const JoinEvent = use('App/Models/JoinEvent');
const Event = use('App/Models/Event');
const Database = use('Database');
class JoinEventRepository {
  constructor () {
    this.joinEvent = JoinEvent;
    this.event = Event;
  }

  /**
   * getDetail
   * @param userId
   * @param eventId
   * @return {Promise.<null>}
   */
  async getDetail(userId, eventId) {
    const event = await this.joinEvent
      .query()
      .where('user_id', userId)
      .where('event_id', eventId)
      .first();
    return event ? event.toJSON() : null;
  }

  /**
   * joinEvent
   * @param params
   * @param userId
   * @param eventCode
   * @return {Promise.<void>}
   */
  async joinToEvent(params, userId, eventCode) {
    const db = await Database.beginTransaction();
    try {
      const joinEvent = new JoinEvent();
      joinEvent.event_id = params.event_id;
      joinEvent.user_id = userId;
      joinEvent.event_code = eventCode;
      joinEvent.share_code = params.share_code ? params.share_code : null;
      await joinEvent.save();
      const event = await this.event.find(params.event_id);
      event.total += 1;
      await event.save();
      await db.commit();
      return joinEvent.toJSON();
    } catch (err) {
      db.rollback();
      return null;
    }
  }

  /**
   * getJoinEventByShareCode
   * @param shareCode
   * @return {Promise.<null>}
   */
  async getJoinEventByShareCode(shareCode) {
    const joinEvent = await this.joinEvent
      .query()
      .where('event_code', shareCode)
      .first();
    return joinEvent ? joinEvent.toJSON() : null;
  }

  /**
   * getTotal
   * @return {Promise.<*>}
   */
  async getTotal() {
    const total = await this.joinEvent.getCount();
    return total;
  }

  /**
   * getListParticipants
   * @param eventId
   * @return {Promise.<Array>}
   */
  async getListParticipants(eventId) {
    const joinEvents = await this.joinEvent
      .query()
      .where('event_id', eventId)
      .with('user', (builder) => {
        builder.select(['id', 'avatar', 'username']);
      })
      .fetch();
    return joinEvents ? joinEvents.toJSON() : [];
  }

  async getPoint(shareCode) {
    const point = await this.joinEvent
      .query()
      .where('share_code', shareCode)
      .getCount();
    return point + 1;
  }

  /**
   * searchParticipants
   * @param eventId
   * @param content
   * @return {Promise.<Array>}
   */
  async searchParticipants(eventId, content) {
    const joinEvents = await this.joinEvent
      .query()
      .with('user', (builder) => {
        builder.where('username', 'like', `%${content}%`);
      })
      .where('event_id', eventId)
      .fetch();
    return joinEvents ? joinEvents.toJSON() : [];
  }

  async getListEventYouJoined(userId) {
    const events = await this.joinEvent
      .query()
      .where('user_id', userId)
      .fetch();
    return events ? events.toJSON() : [];
  }

  /**
   * usePoint
   * @param shareCode
   * @return {Promise.<boolean>}
   */
  async usePoint (shareCode) {
    try {
      await this.joinEvent
        .query()
        .where('share_code', shareCode)
        .update({
          is_used: 1,
        });
      return true;
    } catch (err) {
      return false;
    }
  }
}

module.exports = JoinEventRepository;

