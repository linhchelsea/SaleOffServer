'use strict';

const Post = use('App/Models/Post');
const Config = use('Config');
const { pagination } = Config.get('global');
const { getDayOffsetTime } = use('App/Helpers');
const dayOffsetTime = getDayOffsetTime();
const newsFeedSelector = [
  'posts.id',
  'posts.title',
  'posts.sale_percent',
  'start_date',
  'end_date',
  'like',
  'comment',
  'product_id',
  'posts.created_at',
  'posts.shop_id',
  'posts.user_id',
  'posts.cover',
  'address',
  'is_share',
  'is_event',
  'post_id',
];
const shopSelector = ['id', 'name', 'avatar'];
const userSelector = ['id', 'username', 'avatar'];
class HomeRepository {
  constructor () {
    this.post = Post;
  }

  /**
   * getFollowingNewsFeed
   * @param userId
   * @param productIds
   * @param lastId
   * @param address
   * @returns {Promise.<Array>}
   */
  async getFollowingNewsFeed(userId, productIds, lastId, address) {
    let posts;
    if (productIds.length === 0) {
      posts = await this.post
        .query()
        .innerJoin('shop_follows', 'posts.shop_id', 'shop_follows.shop_id')
        .whereNot('posts.shop_id', null)
        .whereNot('posts.user_id', userId)
        .where('posts.address', 'like', `%${address}%`)
        .where('posts.id', '<', lastId)
        .where('shop_follows.user_id', userId)
        .where('posts.created_at', '>=', dayOffsetTime)
        .where('is_trust', 1)
        .select(newsFeedSelector)
        .with('shop', (builder) => {
          builder.select(shopSelector);
        })
        .with('user', (builder) => {
          builder.select(userSelector);
        })
        .union(function () {
          this.innerJoin('user_follows', 'posts.user_id', 'user_follows.user_two_id')
            .whereNull('posts.shop_id')
            .whereNot('posts.user_id', userId)
            .where('posts.address', 'like', `%${address}%`)
            .where('posts.id', '<', lastId)
            .where('user_follows.user_one_id', userId)
            .where('posts.created_at', '>=', dayOffsetTime)
            .where('is_trust', 1)
            .select(newsFeedSelector);
        })
        .orderBy('id', 'desc')
        .limit(pagination)
        .fetch();
    } else {
      posts = await this.post
        .query()
        .innerJoin('shop_follows', 'posts.shop_id', 'shop_follows.shop_id')
        .whereNot('posts.shop_id', null)
        .whereNot('posts.user_id', userId)
        .whereIn('product_id', productIds)
        .where('posts.address', 'like', `%${address}%`)
        .where('posts.id', '<', lastId)
        .where('shop_follows.user_id', userId)
        .where('posts.created_at', '>=', dayOffsetTime)
        .where('is_trust', 1)
        .select(newsFeedSelector)
        .with('shop', (builder) => {
          builder.select(shopSelector);
        })
        .with('user', (builder) => {
          builder.select(userSelector);
        })
        .union(function () {
          this.innerJoin('user_follows', 'posts.user_id', 'user_follows.user_two_id')
            .whereNull('posts.shop_id')
            .whereNot('posts.user_id', userId)
            .where('posts.address', 'like', `%${address}%`)
            .where('posts.id', '<', lastId)
            .whereIn('product_id', productIds)
            .where('user_follows.user_one_id', userId)
            .where('posts.created_at', '>=', dayOffsetTime)
            .where('is_trust', 1)
            .select(newsFeedSelector);
        })
        .orderBy('id', 'desc')
        .limit(pagination)
        .fetch();
    }
    return posts ? posts.toJSON() : [];
  }

  /**
   * getFollowingNewsFeedCare
   * @param userId
   * @param productIds
   * @param lastId
   * @param address
   * @return {Promise.<Array>}
   */
  async getFollowingNewsFeedCare(userId, productIds, lastId, address) {
    const posts = await this.post
      .query()
      .innerJoin('shop_follows', 'posts.shop_id', 'shop_follows.shop_id')
      .whereNot('posts.shop_id', null)
      .whereNot('posts.user_id', userId)
      .whereIn('product_id', productIds)
      .where('posts.address', 'like', `%${address}%`)
      .where('posts.id', '<', lastId)
      .where('shop_follows.user_id', userId)
      .where('posts.created_at', '>=', dayOffsetTime)
      .where('is_trust', 1)
      .select(newsFeedSelector)
      .with('shop', (builder) => {
        builder.select(shopSelector);
      })
      .with('user', (builder) => {
        builder.select(userSelector);
      })
      .union(function () {
        this.innerJoin('user_follows', 'posts.user_id', 'user_follows.user_two_id')
          .whereNull('posts.shop_id')
          .whereNot('posts.user_id', userId)
          .where('posts.address', 'like', `%${address}%`)
          .where('posts.id', '<', lastId)
          .whereIn('product_id', productIds)
          .where('user_follows.user_one_id', userId)
          .where('posts.created_at', '>=', dayOffsetTime)
          .where('is_trust', 1)
          .select(newsFeedSelector);
      })
      .orderBy('id', 'desc')
      .limit(pagination)
      .fetch();
    return posts ? posts.toJSON() : [];
  }

  /**
   * getNotFollowingNewsFeed
   * @param userId
   * @param shopIds
   * @param userIds
   * @param productIds
   * @param lastId
   * @param address
   * @returns {Promise.<Array>}
   */
  async getNotFollowingNewsFeed(userId, shopIds, userIds, productIds, lastId, address) {
    let posts;
    if (productIds.length === 0) {
      posts = await this.post
        .query()
        .whereNot('shop_id', null)
        .whereNotIn('shop_id', shopIds)
        .whereNot('user_id', userId)
        .where('posts.address', 'like', `%${address}%`)
        .where('id', '<', lastId)
        .where('is_trust', 1)
        .where('created_at', '>=', dayOffsetTime)
        .select(newsFeedSelector)
        .with('shop', (builder) => {
          builder.select(shopSelector);
        })
        .with('user', (builder) => {
          builder.select(userSelector);
        })
        .union(function () {
          this.whereNull('shop_id')
            .whereNotIn('user_id', userIds)
            .whereNot('user_id', userId)
            .where('posts.address', 'like', `%${address}%`)
            .where('id', '<', lastId)
            .where('created_at', '>=', dayOffsetTime)
            .select(newsFeedSelector);
        })
        .orderBy('created_at', 'desc')
        .limit(pagination)
        .fetch();
    } else {
      posts = await this.post
        .query()
        .whereNot('shop_id', null)
        .whereNotIn('shop_id', shopIds)
        .whereNot('user_id', userId)
        .where('posts.address', 'like', `%${address}%`)
        .where('id', '<', lastId)
        .whereIn('product_id', productIds)
        .where('is_trust', 1)
        .where('created_at', '>=', dayOffsetTime)
        .select(newsFeedSelector)
        .with('shop', (builder) => {
          builder.select(shopSelector);
        })
        .with('user', (builder) => {
          builder.select(userSelector);
        })
        .union(function () {
          this.whereNull('shop_id')
            .whereNotIn('user_id', userIds)
            .whereNot('user_id', userId)
            .where('posts.address', 'like', `%${address}%`)
            .where('id', '<', lastId)
            .whereIn('product_id', productIds)
            .where('created_at', '>=', dayOffsetTime)
            .select(newsFeedSelector);
        })
        .orderBy('created_at', 'desc')
        .limit(pagination)
        .fetch();
    }
    return posts ? posts.toJSON() : [];
  }

  /**
   * getNotFollowingNewsFeedCare
   * @param userId
   * @param shopIds
   * @param userIds
   * @param productIds
   * @param lastId
   * @param address
   * @return {Promise.<Array>}
   */
  async getNotFollowingNewsFeedCare(userId, shopIds, userIds, productIds, lastId, address) {
    const posts = await this.post
      .query()
      .whereNot('shop_id', null)
      .whereNotIn('shop_id', shopIds)
      .whereNot('user_id', userId)
      .where('posts.address', 'like', `%${address}%`)
      .where('id', '<', lastId)
      .whereIn('product_id', productIds)
      .where('is_trust', 1)
      .where('created_at', '>=', dayOffsetTime)
      .select(newsFeedSelector)
      .with('shop', (builder) => {
        builder.select(shopSelector);
      })
      .with('user', (builder) => {
        builder.select(userSelector);
      })
      .union(function () {
        this.whereNull('shop_id')
          .whereNotIn('user_id', userIds)
          .whereNot('user_id', userId)
          .where('posts.address', 'like', `%${address}%`)
          .where('id', '<', lastId)
          .whereIn('product_id', productIds)
          .where('created_at', '>=', dayOffsetTime)
          .select(newsFeedSelector);
      })
      .orderBy('created_at', 'desc')
      .limit(pagination)
      .fetch();
    return posts ? posts.toJSON() : [];
  }

  /**
   * getLastPost
   * @returns {Promise.<void>}
   */
  async getLastPost() {
    const post = await this.post.query().orderBy('id', 'desc').first();
    return post ? post.id : 0;
  }
}

module.exports = HomeRepository;
