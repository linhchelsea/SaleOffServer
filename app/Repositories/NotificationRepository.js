'use strict';

const { getNotifications, deleteNotification } = use('App/Helpers');
class NotificationRepository {
  /**
   * get
   * @param userId
   * @returns {Promise.<*>}
   */
  async get(userId) {
    const notifications = await getNotifications(userId);
    return notifications;
  }

  /**
   * deleteNotification
   * @param id
   */
  delete(id) {
    deleteNotification(id);
  }
}

module.exports = NotificationRepository;
