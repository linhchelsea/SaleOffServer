'use strict';

const Token = use('App/Models/Token');
const tokenSelector = [
  'id',
  'device_token',
  'platform',
  'user_id',
];
class TokenRepository {
  constructor () {
    this.token = Token;
  }

  /**
   * getDeviceTokens
   * @param userId
   * @returns {Promise.<Array>}
   */
  async getDeviceTokens(userId) {
    const tokens = await this.token
      .query()
      .where('user_id', userId)
      .where('is_revoke', 0)
      .select(tokenSelector)
      .orderBy('id', 'desc')
      .first();
    return tokens ? tokens.toJSON() : [];
  }

  async getDeviceTokensByIds(ids) {
    const tokens = await this.token
      .query()
      .whereIn('user_id', ids)
      .where('is_revoke', 0)
      .select(tokenSelector)
      .orderBy('id', 'desc')
      .fetch();
    return tokens ? tokens.toJSON() : [];
  }

  /**
   * revokeToken
   * @param userId
   * @return {Promise.<boolean>}
   */
  async revokeToken(userId) {
    try {
      await this.token
        .query()
        .where('user_id', userId)
        .update({ is_revoke: true });
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }

  async getTotalLogin(userId) {
    const total = await this.token
      .query()
      .where('user_id', userId)
      .getCount();
    return +total;
  }
}

module.exports = TokenRepository;
