'use strict';

const Report = use('App/Models/Report');

class ReportRepository {
  constructor () {
    this.report = Report;
  }

  /**
   * send
   * @param userId
   * @param params
   * @returns {Promise.<void>}
   */
  async send(userId, params) {
    await this.report.create({
      type: +params.type,
      reason: +params.reason,
      user_id: userId,
      target_id: +params.target_id,
    });
  }
}

module.exports = ReportRepository;
