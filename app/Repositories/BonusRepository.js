'use strict';

const Bonus = use('App/Models/Bonus');
class BonusRepository {
  /**
   * add
   * @param userOneId
   * @param userTwoId
   * @param shopId
   * @return {Promise.<void>}
   */
  async add(userOneId, userTwoId, shopId) {
    const bonus = new Bonus();
    bonus.user_one_id = userOneId;
    bonus.user_two_id = userTwoId;
    bonus.shop_id = shopId;
    await bonus.save();
  }

  /**
   * check
   * @param userOneId
   * @param userTwoId
   * @param shopId
   * @return {Promise.<null>}
   */
  async check (userOneId, userTwoId, shopId) {
    const bonus = await Bonus
      .query()
      .where('user_one_id', userOneId)
      .where('user_two_id', userTwoId)
      .where('shop_id', shopId)
      .first();
    return bonus ? bonus.toJSON() : null;
  }


  /**
   * getShareMembersByCode
   * @param code
   * @param shopId
   * @return {Promise.<Array>}
   */
  async getShareMembersByCode(shopId) {
    const users = await Bonus
      .query()
      .with('user')
      .where('shop_id', shopId)
      .fetch();
    return users ? users.toJSON() : [];
  }

  /**
   * getPoint
   * @param userOneId
   * @return {Promise.<*>}
   */
  async getPoint(userOneId) {
    const total = await Bonus
      .query()
      .where('user_one_id', userOneId)
      .where('is_used', 0)
      .getCount();
    return total;
  }

  /**
   * useBonus
   * @param shopId
   * @param userOneId
   * @param point
   * @return {Promise.<void>}
   */
  async useBonus(shopId, userOneId, point) {
    await Bonus.query()
      .where('user_one_id', userOneId)
      .where('shop_id', shopId)
      .limit(point)
      .update({ is_used: 1 });
  }
}

module.exports = BonusRepository;

