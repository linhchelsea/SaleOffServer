'use strict';

const Event = use('App/Models/Event');
const Config = use('Config');
const { pagination } = Config.get('global');
const { formatDate, formatDateTime } = use('App/Helpers');
const eventSelector = [
  'id',
  'name',
  'detail',
  'shop_id',
  'images',
  'address',
  'status',
  'created_at',
  'updated_at',
];
class EventRepository {
  constructor () {
    this.event = Event;
  }

  /**
   * getDetail
   * @param id
   * @return {Promise.<null>}
   */
  async getDetail(id) {
    const event = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('id', id)
      .first();
    return event ? event.toJSON() : null;
  }

  /**
   * getEventNewsFeed
   * @param id
   * @return {Promise.<null>}
   */
  async getEventNewsFeed (id) {
    const event = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('id', id)
      .first();
    return event ? event.toJSON() : null;
  }
  /**
   * getShopEvents
   * @param shopId
   * @return {Promise.<Array>}
   */
  async getShopEvents(shopId) {
    const events = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('shop_id', shopId)
      .select(eventSelector)
      .fetch();
    return events ? events.toJSON() : [];
  }

  /**
   * getListEventYouJoined
   * @param ids
   * @return {Promise.<Array>}
   */
  async getListEventYouJoined(ids) {
    const events = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .whereIn('id', ids)
      .fetch();
    return events ? events.toJSON() : [];
  }

  /**
   * getRecentlyEvents
   * @return {Promise.<Array>}
   */
  async getEventsChuaDienRa(page) {
    const date = new Date();
    date.setHours(date.getHours() + 7);
    const events = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('from', '>', date)
      .orderBy('from', 'desc')
      .paginate(page, pagination);
    return events ? events.toJSON().data : [];
  }

  async getEventsDangDienRa(page) {
    const date = new Date();
    date.setHours(date.getHours() + 7);
    const events = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('from', '<=', date)
      .where('to', '>=', date)
      .orderBy('from', 'desc')
      .paginate(page, pagination);
    return events ? events.toJSON().data : [];
  }

  async getEventsDaDienRa(page) {
    const date = new Date();
    date.setHours(date.getHours() + 7);
    const events = await this.event
      .query()
      .with('shop', (builder) => {
        builder.select(['id', 'name', 'avatar']);
      })
      .where('to', '<', date)
      .orderBy('from', 'desc')
      .paginate(page, pagination);
    return events ? events.toJSON().data : [];
  }

  /**
   * createEvent
   * @param params
   * @return {Promise.<*>}
   */
  async createEvent(params) {
    const event = new Event();
    event.shop_id = params.shop_id;
    event.name = params.name;
    event.detail = params.detail;
    event.from = params.from;
    event.to = params.to;
    event.status = 1;
    event.images = JSON.stringify(params.images);
    event.address = params.address;
    await event.save();
    return event.toJSON();
  }

  /**
   * getLastEvent
   * @param shopId
   * @return {Promise.<*>}
   */
  async getLastEvent(shopId) {
    const event = await this.event
      .query()
      .where('shop_id', shopId)
      .orderBy('id', 'desc')
      .first();
    return event;
  }

  /**
   * getDateEvent
   * @return {Promise.<void>}
   */
  async getDateEvent () {
    let homNay = new Date();
    let ngayMai = new Date(+new Date() + 86400000);
    homNay = formatDate(homNay);
    ngayMai = formatDate(ngayMai);
    const toDay = new Date(homNay);
    const tomorrow = new Date(ngayMai);
    const events = await this.event
      .query()
      .where('from', '>=', toDay)
      .where('from', '<=', tomorrow)
      .fetch();
    return events ? events.toJSON() : [];
  }
}

module.exports = EventRepository;

