'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { userNotFound } = Config.get('error');
class SystemException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: userNotFound.message,
      error: userNotFound.error,
    });
  }
}

module.exports = SystemException;
