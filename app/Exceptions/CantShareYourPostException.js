'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { cantShareYourPost } = Config.get('error');
class CantShareYourPostException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: cantShareYourPost.message,
      error: cantShareYourPost.error,
    });
  }
}

module.exports = CantShareYourPostException;
