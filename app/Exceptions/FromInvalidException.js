'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { fromInvalid } = Config.get('error');
class FromInvalidException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: fromInvalid.message,
      error: fromInvalid.error,
    });
  }
}

module.exports = FromInvalidException;
