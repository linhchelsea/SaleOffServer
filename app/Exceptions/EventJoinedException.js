'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { joinedEvent } = Config.get('error');
class EventJoinedException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: joinedEvent.message,
      error: joinedEvent.error,
    });
  }
}

module.exports = EventJoinedException;
