'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { shopNotActive } = Config.get('error');
class ShopNotActiveException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: shopNotActive.message,
      error: shopNotActive.error,
    });
  }
}

module.exports = ShopNotActiveException;
