'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { eventNotFound } = Config.get('error');
class EventNotFoundException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: eventNotFound.message,
      error: eventNotFound.error,
    });
  }
}

module.exports = EventNotFoundException;
