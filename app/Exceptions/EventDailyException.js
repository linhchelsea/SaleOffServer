'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

const Config = use('Config');
const { dailyEvent } = Config.get('error');
class EventDailyException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(401).send({
      status: 401,
      data: null,
      message: dailyEvent.message,
      error: dailyEvent.error,
    });
  }
}

module.exports = EventDailyException;
