'use strict';

const Controller = use('App/Controllers/Controller');
const NotificationService = use('App/Services/NotificationService');
class NotificationController extends Controller {
  constructor () {
    super();
    this.notificationService = new NotificationService();
  }

  /**
   * get
   * @param auth
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async get({ auth }) {
    const userId = +auth.user.id;
    const notifications = await this.notificationService.get(userId);
    const data = {
      notifications,
    };
    return this.buildSuccess({ data });
  }

  /**
   * deleteNotification
   * @param params
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async deleteNotification({ params }) {
    const { id } = params;
    this.notificationService.delete(id);
    return this.buildSuccess({});
  }
}

module.exports = NotificationController;
