'use strict';

const Controller = use('App/Controllers/Controller');
const ShopService = use('App/Services/ShopService');
const ShopFollowService = use('App/Services/ShopFollowService');
const Config = use('Config');
const error = Config.get('error');
class ShopController extends Controller {
  constructor() {
    super();
    this.shopService = new ShopService();
    this.shopFollowService = new ShopFollowService();
  }

  /**
   * create
   * @param request
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async create({ request, auth }) {
    const user = await auth.getUser();
    const params = request.all();
    const shop = await this.shopService.createShop(user.id, params);
    const data = {
      shop,
    };
    return this.buildSuccess({ data });
  }

  /**
   * updateInfo
   * @param params
   * @param auth
   * @param request
   * @param response
   * @return {Promise.<*>}
   */
  async updateInfo({ params, auth, request, response }) {
    const user = await auth.getUser();
    const { id } = params;
    const data = request.only([
      'name', 'phone', 'avatar', 'cover', 'description', 'address', 'web',
    ]);
    const update = await this.shopService.updateInfo(id, data, user.id);
    if (update) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {});
  }

  /**
   * getShopInfo
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getShopInfo({ params, auth }) {
    const userId = +await auth.user.id;
    const shop = await this.shopService.getShopInfo(userId, params.shopId);
    const data = {
      shop,
    };
    return this.buildSuccess({ data });
  }

  /**
   * getYourShopInfo
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getYourShopInfo({ params }) {
    const shop = await this.shopService.getYourShopInfo(params.shopId);
    const data = {
      shop,
    };
    return this.buildSuccess({ data });
  }

  /**
   * follow shop
   * @param params
   * @param auth
   * @return {Promise.<*>}
   */
  async followShop ({ params, auth, request }) {
    const user = await auth.getUser();
    const { id } = params;
    const { code } = request.only(['code']);
    const follow = await this.shopFollowService.followShop(id, code, user.id);
    if (follow) {
      return this.buildSuccess({});
    }
    return this.errorResponse({});
  }

  /**
   * unfollow shop
   * @param params
   * @param auth
   * @return {Promise.<*>}
   */
  async unfollowShop({ params, auth }) {
    const user = await auth.getUser();
    const shopId = params.id;
    const follow = await this.shopFollowService.unfollowShop(shopId, user.id);
    if (follow) {
      return this.buildSuccess({});
    }
    return this.errorResponse({});
  }

  /**
   * get list shop to follow
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getListFollowSearch({ auth }) {
    const user = await auth.getUser();
    const shops = await this.shopFollowService.getListFollowSearch(user);
    const data = {
      shops,
    };
    return this.buildSuccess({ data });
  }

  /**
   * getListShopFollowed
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getListShopFollowed({ auth }) {
    const user = await auth.getUser();
    const shops = await this.shopService.getListShopFollowed(user.id);
    const data = {
      shops,
    };
    return this.buildSuccess({ data });
  }

  /**
   * getYourShops
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getYourShops({ auth }) {
    const user = await auth.getUser();
    const shops = await this.shopService.getYourShops(user.id);
    const data = {
      shops,
    };
    return this.buildSuccess({ data });
  }

  /**
   * getYourShopsWorking
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getYourShopsWorking({ auth }) {
    const user = await auth.getUser();
    const shops = await this.shopService.getYourShopsWorking(user.id);
    const data = {
      shops,
    };
    return this.buildSuccess({ data });
  }

  /**
   * rate
   * @param request
   * @param auth
   * @param params
   * @param response
   * @return {Promise.<*>}
   */
  async rate({ request, auth, params, response }) {
    const { point } = request.all();
    const user = await auth.getUser();
    const { id } = params;
    const isRate = await this.shopService.rateShop(id, user.id, point);
    if (isRate) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {
      message: error.rate_shop_fail.message,
      error: error.rate_shop_fail.error,
    });
  }

  /**
   * accept
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async accept({ params, request, response }) {
    const { id } = params;
    const { adminId } = request.only(['adminId']);
    const accept = await this.shopService.accept(id, adminId);
    if (accept) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {});
  }

  /**
   * reject
   * @param params
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async reject({ params }) {
    const { id } = params;
    await this.shopService.reject(id);
    return this.buildSuccess({});
  }

  /**
   * getDeleteShop
   * @param response
   * @param params
   * @return {Promise.<*>}
   */
  async getDeleteShop({ response, params }) {
    const { id } = params;
    const isDelete = await this.shopService.requestDeleteShop(id);
    if (isDelete) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {});
  }

  /**
   * sendDeleteShopNotification
   * @param request
   * @return {Promise.<void>}
   */
  async sendDeleteShopNotification({ request }) {
    const { shop, ids } = request.only(['shop', 'ids']);
    this.shopService.sendDeleteShopNotification(shop, ids);
  }

  async getShareMembers({ auth, params }) {
    const userId = +auth.user.id;
    const { id } = params;
    const users = await this.shopService.getShareMembers(userId, id);
    const data = {
      users,
    };
    return this.buildSuccess({ data });
  }

  async useBonus ({ auth, params, request }) {
    const userId = +auth.user.id;
    const { id } = params;
    const data = request.only(['user_id', 'point']);
    await this.shopService.useBonus(userId, id, data);
    return this.buildSuccess({ });
  }
}

module.exports = ShopController;
