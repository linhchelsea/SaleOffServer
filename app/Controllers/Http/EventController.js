'use strict';

const Controller = use('App/Controllers/Controller');
const EventService = use('App/Services/EventService');
class EventController extends Controller {
  constructor() {
    super();
    this.eventService = new EventService();
  }

  /**
   * getDetail
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getDetail({ auth, params }) {
    const { id } = params;
    const userId = +auth.user.id;
    const event = await this.eventService.getDetail(id, userId);
    return this.buildSuccess({ data: { event } });
  }

  /**
   * getShopEvents
   * @param auth
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getShopEvents({ auth, params }) {
    const userId = +auth.user.id;
    const { shopId } = params;
    const events = await this.eventService.getShopEvents(userId, shopId);
    const data = {
      events,
    };
    return this.buildSuccess({ data });
  }

  /**
   *
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getYourEvents({ auth }) {
    const userId = +auth.user.id;
    const events = await this.eventService.getEvents(userId);
    const data = {
      events,
    };
    return this.buildSuccess({ data });
  }

  /**
   *
   * @param auth
   * @param params
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getRecentlyEvents({ auth, params }) {
    const userId = +auth.user.id;
    const { page, type } = params;
    const events = await this.eventService.getRecentlyEvents(userId, page, type);
    const data = {
      events,
    };
    return this.buildSuccess({ data });
  }
  /**
   * create
   * @param request
   * @param auth
   * @param response
   * @return {Promise.<*>}
   */
  async create({ request, auth }) {
    const userId = +auth.user.id;
    const params = request.all();
    await this.eventService.createEvent(userId, params);
    return this.buildSuccess({});
  }

  /**
   * joinEvent
   * @param auth
   * @param request
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async joinEvent({ auth, request }) {
    const params = request.only(['event_id', 'share_code', 'description']);
    console.log(params);
    const userId = +auth.user.id;
    const joinEvent = await this.eventService.joinEvent(params, userId);
    const data = {
      share_code: joinEvent.event_code,
    };
    return this.buildSuccess({ data });
  }

  /**
   * getListParticipants
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async getListParticipants({ params }) {
    const { id } = params;
    const participants = await this.eventService.getListParticipants(id);
    const data = {
      participants,
    };
    return this.buildSuccess({ data });
  }

  /**
   * searchParticipants
   * @param params
   * @param request
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async searchParticipants({ params, request }) {
    const { id } = params;
    const { content } = request.only(['content']);
    const participants = await this.eventService.searchParticipants(id, content);
    const data = {
      participants,
    };
    return this.buildSuccess({ data });
  }

  /**
   * useEventPoint
   * @param auth
   * @param params
   * @param response
   * @returns {Promise.<*>}
   */
  async useEventPoint({ auth, params, response }) {
    const authId = +auth.user.id;
    const { id, userId } = params;
    const isUsed = await this.eventService.usePoint(id, userId, authId);
    if (isUsed) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {});
  }
}

module.exports = EventController;
