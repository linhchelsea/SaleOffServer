'use strict';

const Controller = use('App/Controllers/Controller');
const SearchService = use('App/Services/SearchService');
const { formatDate } = use('App/Helpers');
class SearchController extends Controller {
  constructor() {
    super();
    this.searchService = new SearchService();
  }

  /**
   * searchShop
   * @param request
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async searchShop({ request, auth }) {
    const params = request.only(['page', 'text', 'category_id', 'type']);
    const page = params.page > 0 ? params.page : 1;
    const user = await auth.getUser();
    const shops = await this.searchService
      .searchShop(page, params.text, params.category_id, params.type, user.id, user.address);
    const data = {
      shops,
    };
    return this.buildSuccess({ data });
  }

  /**
   * searchUser
   * @param auth
   * @param request
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async searchUser({ auth, request }) {
    const user = await auth.getUser();
    const params = request.only(['page', 'text', 'type']);
    const users = await this.searchService.searchUser(user.id, params, user.address);
    const data = {
      users,
    };
    return this.buildSuccess({ data });
  }

  /**
   * searchPost
   * @param request
   * @param auth
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async searchPost({ request, auth }) {
    const params = request.only(['page', 'text', 'category_id', 'sub_category_id', 'type', 'follow']);
    const page = +params.page > 1 ? params.page : 1;
    const user = await auth.getUser();
    const posts = await this.searchService
      .searchPost(page, params, user.id, user.address);
    posts.forEach((post) => {
      post.start_date = formatDate(post.start_date);
      post.end_date = formatDate(post.end_date);
    });
    const data = {
      posts,
    };
    return this.buildSuccess({ data });
  }
}

module.exports = SearchController;
