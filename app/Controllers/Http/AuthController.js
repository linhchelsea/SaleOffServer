'use strict';

const Controller = use('App/Controllers/Controller');
const AuthService = use('App/Services/AuthService');
const AuthenException = use('App/Exceptions/AuthenException');
class AuthController extends Controller{
  /**
   * constructor
   */
  constructor() {
    super();
    this.authService = new AuthService();
  }

  /**
   * login
   * @param request
   * @returns Promise<*>
   */
  async login({ request, auth }) {
    try {
      const params = request.only([
        'id_token',
        'device_token',
        'platform',
      ]);
      const data = await this.authService.login(params, auth);
      return this.buildSuccess({ data });
    } catch (err) {
      throw new AuthenException();
    }
  }

  /**
   * postLogout
   * @param auth
   * @param response
   * @return {Promise.<*>}
   */
  async postLogout({ auth, response }) {
    const userId = +auth.user.id;
    const isLogout = this.authService.logout(userId);
    if (isLogout) {
      return this.buildSuccess({});
    }
    return this.errorResponse(response, {});
  }
}

module.exports = AuthController;
