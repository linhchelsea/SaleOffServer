'use strict';

const Controller = use('App/Controllers/Controller');
const ReportService = use('App/Services/ReportService');
const Config = use('Config');
const reports = Config.get('reports');
class ReportController extends Controller {
  /**
   * constructor
   */
  constructor() {
    super();
    this.reportService = new ReportService();
  }

  /**
   * get
   * @param params
   * @return {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async get({ params }) {
    const { type } = params;
    let data;
    switch (+type) {
      case 1:
        data = {
          reposts: reports.post,
        };
        break;
      case 2:
        data = {
          reposts: reports.user,
        };
        break;
      default:
        data = {
          reposts: reports.shop,
        };
        break;
    }
    return this.buildSuccess({ data });
  }
  /**
   * send
   * @param auth
   * @param request
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async send({ auth, request }) {
    const userId = +auth.user.id;
    const params = request.all();
    await this.reportService.send(userId, params);
    return this.buildSuccess({});
  }
}

module.exports = ReportController;
