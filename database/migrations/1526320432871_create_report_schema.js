'use strict';

const Schema = use('Schema');

class CreateReportSchema extends Schema {
  up () {
    this.create('reports', (table) => {
      table.bigIncrements();
      table.integer('type');
      table.integer('reason');
      table.bigInteger('user_id').unsigned().references('id').inTable('users');
      table.bigInteger('target_id');
      table.integer('status').default(1);
      table.text('note');
      table.bigInteger('admin_id').unsigned().references('id').inTable('admins');
      table.timestamps();
    });
  }

  down () {
    this.drop('reports');
  }
}

module.exports = CreateReportSchema;
