'use strict';

const Schema = use('Schema');

class CreateBonusSchema extends Schema {
  up () {
    this.create('bonuses', (table) => {
      table.bigIncrements();
      table.bigInteger('user_one_id').unsigned().references('id').inTable('users');
      table.bigInteger('user_two_id').unsigned().references('id').inTable('users');
      table.bigInteger('shop_id').unsigned().references('id').inTable('shops');
      table.boolean('is_used').default(0);
      table.timestamps();
    });
  }

  down () {
    this.drop('bonuses');
  }
}

module.exports = CreateBonusSchema;
