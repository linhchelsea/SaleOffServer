'use strict';

const Schema = use('Schema');

class CreateEventSchema extends Schema {
  up () {
    this.create('events', (table) => {
      table.bigIncrements();
      table.bigInteger('shop_id').unsigned().references('id').inTable('shops');
      table.integer('total').default(0);
      table.text('name');
      table.text('detail');
      table.text('images').default(JSON.stringify([]));
      table.dateTime('from');
      table.dateTime('to');
      table.text('address');
      table.integer('status');
      table.timestamps();
    });
  }

  down () {
    this.drop('events');
  }
}

module.exports = CreateEventSchema;
