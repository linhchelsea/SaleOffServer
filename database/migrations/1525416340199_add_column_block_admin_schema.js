'use strict';

const Schema = use('Schema');

class AddColumnBlockAdminSchema extends Schema {
  up () {
    this.table('admins', (table) => {
      table.boolean('is_block').default(0);
    })
  }

  down () {
    this.table('admins', (table) => {
      table.dropColumn('is_block');
    })
  }
}

module.exports = AddColumnBlockAdminSchema
