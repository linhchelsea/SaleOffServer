'use strict';

const Schema = use('Schema');

class AddColumnIsEventPostSchema extends Schema {
  up () {
    this.table('posts', (table) => {
      table.boolean('is_event').default(0);
    });
  }

  down () {
    this.table('posts', (table) => {
      table.dropTable('is_event');
    });
  }
}

module.exports = AddColumnIsEventPostSchema;
