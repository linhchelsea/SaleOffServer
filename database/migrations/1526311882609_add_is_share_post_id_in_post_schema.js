'use strict';

const Schema = use('Schema');

class AddIsSharePostIdInPostSchema extends Schema {
  up () {
    this.table('posts', (table) => {
      table.boolean('is_share').default(false);
      table.bigInteger('post_id');
    });
  }

  down () {
    this.table('posts', (table) => {
      table.dropColumn('is_share');
      table.dropColumn('post_id');
    });
  }
}

module.exports = AddIsSharePostIdInPostSchema;
