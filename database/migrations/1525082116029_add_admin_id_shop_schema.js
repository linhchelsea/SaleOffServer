'use strict';

const Schema = use('Schema');

class AddAdminIdShopSchema extends Schema {
  up () {
    this.table('shops', (table) => {
      table.bigInteger('admin_id').unsigned().references('id').inTable('admins');
    });
  }

  down () {
    this.table('shops', (table) => {
      table.dropColumn('admin_id');
    });
  }
}

module.exports = AddAdminIdShopSchema;
