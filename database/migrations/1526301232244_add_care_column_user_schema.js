'use strict';

const Schema = use('Schema');

class AddCareColumnUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      const cares = [];
      table.string('cares').default(JSON.stringify(cares));
    });
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('cares');
    });
  }
}

module.exports = AddCareColumnUserSchema;
