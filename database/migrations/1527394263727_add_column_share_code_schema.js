'use strict';

const Schema = use('Schema');

class AddColumnShareCodeSchema extends Schema {
  up () {
    this.table('join_events', (table) => {
      table.integer('share_code');
    });
  }

  down () {
    this.table('join_events', (table) => {
      table.dropTable('share_code');
    });
  }
}

module.exports = AddColumnShareCodeSchema;
