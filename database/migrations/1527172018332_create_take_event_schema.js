'use strict';

const Schema = use('Schema');

class CreateTakeEventSchema extends Schema {
  up () {
    this.create('join_events', (table) => {
      table.bigIncrements();
      table.bigInteger('event_id').unsigned().references('id').inTable('events');
      table.bigInteger('user_id').unsigned().references('id').inTable('users');
      table.integer('event_code');
      table.boolean('status').default(1);
      table.unique(['user_id', 'event_id']);
      table.timestamps();
    });
  }

  down () {
    this.drop('join_events');
  }
}

module.exports = CreateTakeEventSchema;
