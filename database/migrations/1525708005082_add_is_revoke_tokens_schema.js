'use strict';

const Schema = use('Schema');

class AddIsRevokeTokensSchema extends Schema {
  up () {
    this.table('tokens', (table) => {
      table.boolean('is_revoke').default(0);
    });
  }

  down () {
    this.table('tokens', (table) => {
      table.dropColumn('is_revoke');
    });
  }
}

module.exports = AddIsRevokeTokensSchema;
