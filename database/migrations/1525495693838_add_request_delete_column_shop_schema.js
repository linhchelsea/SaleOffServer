'use strict';

const Schema = use('Schema');

class AddRequestDeleteColumnShopSchema extends Schema {
  up () {
    this.table('shops', (table) => {
      table.boolean('request_delete').default(0);
    });
  }

  down () {
    this.table('shops', (table) => {
      table.dropColumn('request_delete');
    });
  }
}

module.exports = AddRequestDeleteColumnShopSchema;
