'use strict';

const Schema = use('Schema');

class AddBonusCodeColumnUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.integer('bonus_code');
    });
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('bonus_code');
    });
  }
}

module.exports = AddBonusCodeColumnUserSchema;
