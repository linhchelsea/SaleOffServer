'use strict';

const Schema = use('Schema');

class AddColumnIsUsedSchema extends Schema {
  up () {
    this.table('join_events', (table) => {
      table.boolean('is_used').default(0);
    });
  }

  down () {
    this.table('join_events', (table) => {
      table.dropColumn('is_used');
    });
  }
}

module.exports = AddColumnIsUsedSchema;
