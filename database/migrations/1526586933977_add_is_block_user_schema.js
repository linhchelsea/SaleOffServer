'use strict';

const Schema = use('Schema');

class AddIsBlockUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('is_block').default(0);
    });
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('is_block');
    });
  }
}

module.exports = AddIsBlockUserSchema;
