module.exports = {
  post: {
    1: 'Bài viết chứa thông tin sai sự thật',
    2: 'Bài viết chứa nội dung không lành mạnh',
    3: 'Bài viết chứa nội dung xuyên tạc, chống phá nhà nước',
    4: 'Bài viết chứa những bình luận mang tính tiêu cực',
  },
  user: {
    1: 'Người dùng này là giả mạo',
    2: 'Người dùng có hành vi quấy rối',
    3: 'Người dùng thường xuyên đăng tải thông tin không lành mạnh',
    4: 'Người dùng có hành vi lừa đảo',
    5: 'Người dùng có những bình luận xuyên tạc'
  },
  shop: {
    1: 'Cửa hàng này là giả mạo',
    2: 'Cửa hàng kinh doanh hàng cấm',
    3: 'Cửa hàng thường xuyên đăng tải thông tin không lành mạnh',
    4: 'Cửa hàng có hành vi lừa đảo',
    5: 'Cửa hàng có những bình luận xuyên tạc'
  }
};
