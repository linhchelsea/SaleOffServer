module.exports = {
  success: {
    error: 0,
    message: 'success'
  },
  auth: {
    error: 1,
    message: 'invalid_token',
  },
  system: {
    error: 2,
    message: 'system_error',
  },
  categoryNotFound: {
    error: 3,
    message: 'category_not_found',
  },
  shopCatNotFound: {
    error: 4,
    message: 'shop_category_not_found',
  },
  shopNotFound: {
    error: 5,
    message: 'shop_not_found',
  },
  shopOwner: {
    error: 6,
    message: 'not_shop_owner',
  },
  userInvalid: {
    error: 7,
    message: 'user_invalid',
  },
  shopFollowed: {
    error: 8,
    message: 'shop_followed',
  },
  CantFollowYourShop: {
    error: 9,
    message: 'cant_follow_your_shop',
  },
  shopUnfollowed: {
    error: 10,
    message: 'shop_unfollowed',
  },
  CantUnfollowYourShop: {
    error: 11,
    message: 'cant_unfollow_your_shop',
  },
  userNotFound: {
    error: 12,
    message: 'user_not_found',
  },
  cantFollowYourSelf: {
    error: 13,
    message: 'cant_follow_yourself',
  },
  userFollowed: {
    error: 14,
    message: 'user_followed',
  },
  cantUnfollowYourSelf: {
    error: 15,
    message: 'cant_unfollow_yourself',
  },
  userUnfollowed: {
    error: 16,
    message: 'user_unfollowed',
  },
  shopRated: {
    error: 17,
    message: 'shop_rated',
  },
  CantRateYourShop: {
    error: 18,
    message: 'cant_rate_your_shop',
  },
  rate_shop_fail: {
    error: 19,
    message: 'rate_shop_fail',
  },
  productNotFound: {
    error: 20,
    message: 'product_not_found',
  },
  imagesTooLong: {
    error: 21,
    message: 'images_too_long',
  },
  notShopMember: {
    error: 22,
    message: 'not_shop_member',
  },
  postNotFound: {
    error: 23,
    message: 'post_not_found',
  },
  postLiked: {
    error: 24,
    message: 'you_liked_this_post',
  },
  postUnliked: {
    error: 25,
    message: 'you_unliked_this_post',
  },
  commentNotFound: {
    error: 26,
    message: 'comment_not_found',
  },
  notYourComment: {
    error: 27,
    message: 'not_your_comment',
  },
  notYourPost: {
    error: 28,
    message: 'not_your_post',
  },
  productNotBelongCat: {
    error: 29,
    message: 'product_is_not_belongs_to_category',
  },
  categoryInvalid: {
    error: 30,
    message: 'category_is_invalid',
  },
  shopNotActive: {
    error: 31,
    message: 'shop_not_active',
  },
  cantRemoveShopOwner: {
    error: 32,
    message: 'cant_remove_shop_owner',
  },
  caresInvalid: {
    error: 33,
    message: 'cares_is_invalid',
  },
  cantShareYourPost: {
    error: 34,
    message: 'cant_share_your_post',
  },
  bonusCodeInvalid: {
    error: 35,
    message: 'bonus_code_is_invalid',
  },
  cantBonusYourSelf: {
    error: 36,
    message: 'cant_bonus_your_self',
  },
  pointInvalid: {
    error: 37,
    message: 'point_is_invalid',
  },
  fromInvalid: {
    error: 38,
    message: 'begin_time_is_over',
  },
  joinedEvent: {
    error: 39,
    message: 'you_joined_this_event',
  },
  shareCodeNotFound: {
    error: 40,
    message: 'share_code_not_found',
  },
  dailyEvent: {
    error: 41,
    message: 'you_can_not_two_events_in_a_day',
  },
  eventNotFound: {
    error: 42,
    message: 'event_not_found',
  },
  notParticipants: {
    error: 43,
    message: 'user_have_not_participated_yet',
  },
};
